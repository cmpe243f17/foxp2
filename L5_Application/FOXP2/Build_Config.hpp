/*
 * Build_Config.hpp
 *
 *  Created on: Oct 11, 2017
 *      Author: Ahsan Uddin
 */

#ifndef BUILD_CONFIG_HPP_
#define BUILD_CONFIG_HPP_

// Build essentials

// If we ever hit an assert, it will display 99!
#define ASSERT_LED_DISPLAY      (99)
#define ASSERT_DEBUG(cond)      if(!cond) {/*LD.setNumber(ASSERT_LED_DISPLAY);*/}


// Global variables that need to be in sync project wide

// All ECUs need to use the following CAN params
#define CAN_BAUDRATE            (100)
#define RECEIVER_QUEUE_SIZE     (100)
#define SENDER_QUEUE_SIZE       (100)
#define CAN_WAIT_VALUE          (0)

// Motor and Servo specifics
// Needed to communicate between Gateway and Motor
// TODO: Maybe embed it in DBC??    - Ahsan 10/21

// Enums for gateway to send message for
// controlling the motor's momentum
typedef enum
{
    MOMENTUM_NOP            = 0x0,
    MOMENTUM_STOP           = 0x1,
    MOMENTUM_FORWARD        = 0x2,
    MOMENTUM_REVERSE        = 0x3,
    MOMENTUM_MAX            = 0x4,
    ABSOLUTE_MOMENTUM_MAX   = 0xF      // Max is 4 bits!
} momentum_E;

// Enums for gateway to send message for
// controlling the servo's direction
typedef enum
{
    TURN_NOP                = 0x0,
    TURN_LEFT               = 0x1,
    TURN_RIGHT              = 0x2,
    TURN_STRAIGHT           = 0x3,
    TURN_MAX                = 0x4,
    ABSOLUTE_TURN_MAX       = 0xF     // Max is 4 bits!
} direction_E ;

enum
{
    GEO_LEFT        = 0x0,
    GEO_RIGHT       = 0x1,
    GEO_STRAIGHT    = 0x2,
    GEO_STOP        = 0x3,
};

// Sensor and Gateway specific
#define SENSOR_PROX_DISTANCE_THRESH_IN 10U  // [inches]

#endif /* BUILD_CONFIG_HPP_ */
