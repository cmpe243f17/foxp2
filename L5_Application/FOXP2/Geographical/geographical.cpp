#include "FreeRTOS.h"
#include "semphr.h"
#include "generated_can.h"
#include "geographical.hpp"
#include "io.hpp"
#include <stdio.h>
#include "can.h"
#include "Build_Config.hpp"
#include <stdbool.h>
#include "geo_sensor.hpp"
#include <string.h>
#include <stdlib.h>
#include <printf_lib.h>

#ifdef ECU_GEOGRAPHICAL

#define MOCK_GEO            0 // mocking values to test if values are actually sent over CAN
#define DEBUG_GEO           0// Line 208: if set to '1', it will function without bridge/app and steer towards hard-coded checkpoint, if '0' it is ignored

#define CAN_HANDLE      	can1


#define DEFAULT_DIRECTION   (1 << GEO_STOP)          // mock values
#define DEFAULT_DISTANCE 	0.000000
#define MOCK_DIRECTION_0    20
#define MOCK_DISTANCE_0		20.000000
#define MOCK_DIRECTION_1    10
#define MOCK_DISTANCE_1		100.101101

#define TEST_LAT_0          37.296102  //home
#define TEST_LONG_0         -121.959970

#define TEST_LAT_1          37.336004  // in front of SU
#define TEST_LONG_1         -121.881783

#define TEST_LAT_2          37.337199   // by the ATM machine at school
#define TEST_LONG_2         -121.879869

#define DEFAULT_MIA_MS      1000

#define ALL_CHECKPOINT_REACHED  -1      // returned index value when all checkpoint is reached
#define MIN_DISTANCE_LEFT   5           // minimum distance radius? to mark as checkpoint reached
#define MAX_LEFT_LIMIT      -160
#define MAX_RIGHT_LIMIT     200
#define SLIGHT_RIGHT_LIMIT  -35
#define SLIGHT_LEFT_LIMIT   15

typedef enum 
{
    idle                        = 0,
    geo_receive_lat_long        = 1,
    geo_num_checkpoint_received = 2
}geo_bridge_handshake_sm;


typedef struct 
{
    float latitude;
    float longitude;

    bool reached_checkpoint;
}checkpoint_t;

// Globals
const uint32_t                  BRIDGE_STOP__MIA_MS             = DEFAULT_MIA_MS;
const BRIDGE_STOP_t             BRIDGE_STOP__MIA_MSG            = {0};
const uint32_t                  BRIDGE_NUM_CHECKPOINT__MIA_MS   = DEFAULT_MIA_MS;
const BRIDGE_NUM_CHECKPOINT_t   BRIDGE_NUM_CHECKPOINT__MIA_MSG  = {0};
const uint32_t                  BRIDGE_LAT_LONG__MIA_MS         = DEFAULT_MIA_MS;
const BRIDGE_LAT_LONG_t         BRIDGE_LAT_LONG__MIA_MSG        = {0.000000};
const uint32_t                  BRIDGE_DONE__MIA_MS             = DEFAULT_MIA_MS;
const BRIDGE_DONE_t             BRIDGE_DONE__MIA_MSG            = {0};
const uint32_t                  BRIDGE_GO__MIA_MS               = DEFAULT_MIA_MS;
const BRIDGE_GO_t               BRIDGE_GO__MIA_MSG              = {0};

// sending these CAN messages
static GEOGRAPHICAL_CMD_t geo_cmd = {.GEOGRAPHICAL_CMD_direction = DEFAULT_DIRECTION,
                                    .GEOGRAPHICAL_CMD_distance = DEFAULT_DISTANCE};
static GEOGRAPHICAL_RESP_NUM_t send_checkpoint_received_cmd = {0};
static GEOGRAPHICAL_CURR_LAT_LONG_t curr_lat_long = {0};
static COMPASS_VALUE_CMD_t heading_value = {0};
static GEO_ALERT_t geo_alerts = {0};
static GPS_VALUE_CMD_t gps_data = {0};

// receiving these CAN messages
static BRIDGE_STOP_t stop_cmd = {0};
static BRIDGE_NUM_CHECKPOINT_t num_checkpoint_cmd = {0};
static BRIDGE_LAT_LONG_t lat_long_cmd = {0};
static BRIDGE_DONE_t done_cmd = {0};
static BRIDGE_GO_t go_cmd = {0};

float e_heading;
float e_bearing;
float e_distance;
float e_curr_latitude;
float e_curr_longitude;
float e_checkpoint_latitude;    // extern variables that will store coordinates from bridge
float e_checkpoint_longitude;  // extern variables that will store coordinates from bridge
uint8_t e_heading_valid;
bool e_gps_distance_valid;

// geo_bridge handshake variables
static uint8_t s_bridge_num_checkpoint = 0;
static uint8_t s_checkpoint_lat_long_count = 0;
static checkpoint_t *s_checkpoint_data = NULL;
static bool s_bridge_send_done_long_lat = false;
static bool s_send_go_to_master = false;
static bool s_send_stop_to_master = false;
static int s_closest_checkpoint_index = 0;
static bool s_closest_checkpoint_found = false;

static uint8_t s_checkpoints_left = 0;
static uint8_t s_num_of_checkpoints = 0;

static bool setup_can_filter(void);
static void can_rx(void);
static void can_rx_mia_handler(void);
static bool geo_bridge_handshake(bool sent_checkpoints_done);
static int find_closest_checkpoint(checkpoint_t *points, int size);
static bool command_car(bool send_stop, bool send_go);

void geographical_init(void)
{
    bool success = false;

    success = CAN_init(CAN_HANDLE, CAN_BAUDRATE, RECEIVER_QUEUE_SIZE, SENDER_QUEUE_SIZE, NULL, NULL);

    if(success)
    {
        // set up CAN filter
        success = setup_can_filter();
    }

    if(success)
    {
        // Make node (ECU) active on CAN bus by sending reset
        CAN_reset_bus(CAN_HANDLE);
#if !MOCK_GEO
        // Initialize GPS and Compass module
        success = geo_sensor_init();
#endif
    }
    else
    {
        puts("CAN driver init failed!!!!!!!!!!! FIX ME");
    }

    if(!success)
    {
        puts("CAN or GEO SENSOR init failed!!!!! FIX ME");
    }

}

void periodic_geographical_1HZ(void)
{
    if (CAN_is_bus_off(CAN_HANDLE)) 
    {
        CAN_reset_bus(CAN_HANDLE);
    }

    // LE.toggle(1);

}

void periodic_geographical_10HZ(void)
{

#if MOCK_GEO
	if(SW.getSwitch(1))
	{
		puts("pressed 1");
		geo_cmd.GEOGRAPHICAL_CMD_direction = MOCK_DIRECTION_0;
		geo_cmd.GEOGRAPHICAL_CMD_distance = MOCK_DISTANCE_0;
        curr_lat_long.GEOGRAPHICAL_CURR_LAT = TEST_LAT_0;
        curr_lat_long.GEOGRAPHICAL_CURR_LONG = TEST_LONG_0;
	}
	else if(SW.getSwitch(2))
	{
		puts("pressed 2");
		geo_cmd.GEOGRAPHICAL_CMD_direction = MOCK_DIRECTION_1;
		geo_cmd.GEOGRAPHICAL_CMD_distance = MOCK_DISTANCE_1;
        curr_lat_long.GEOGRAPHICAL_CURR_LAT = TEST_LAT_1;
        curr_lat_long.GEOGRAPHICAL_CURR_LONG = TEST_LONG_1;
	}

#else
    geo_sensor_run();

    bool success = command_car(s_send_stop_to_master, s_send_go_to_master);
    if(!success)
    {
        // go sent w/out checkpoints, send alert to app
        // '1' means go sent w/out checkpoints
        geo_alerts.GEO_ALERT_GO_NO_CHECKPOINTS = 1;
    }
    else
    {
        geo_alerts.GEO_ALERT_GO_NO_CHECKPOINTS = 0;
    }

    geo_bridge_handshake(s_bridge_send_done_long_lat);


    heading_value.COMPASS_VALUE_CMD_VALID = e_heading_valid;
    heading_value.COMPASS_VALUE_CMD_VALUE = e_heading;

    gps_data.GPS_VALUE_CMD_VALID = 1;
    gps_data.GPS_VALUE_CMD_BEARING = e_bearing;
    gps_data.GPS_VALUE_CMD_DISTANCE = e_distance;

    geo_cmd.GEOGRAPHICAL_CMD_distance = e_distance;
#if DEBUG_GEO
    // This portion is only for debug only, for without bridge communication

    // u0_dbg_printf("h: %f b:%f d:%f\r\n", e_heading, e_bearing, e_distance);
    // u0_dbg_printf("la: %f lo: %f\r\n" , e_curr_latitude, e_curr_longitude);
    // u0_dbg_printf(" d: %f dist: %f\r\n", difference, e_distance);
    float difference = e_heading - e_bearing;
    char dir = '\0';
    if (difference <= MAX_LEFT_LIMIT)
    {
        geo_cmd.GEOGRAPHICAL_CMD_direction = (1 << GEO_LEFT);
        dir = 'L';
        // puts("max left");
    }
    else if (difference >= MAX_RIGHT_LIMIT) 
    {  
        geo_cmd.GEOGRAPHICAL_CMD_direction = (1 << GEO_RIGHT);
        dir = 'R';
        // puts("max right");
    }
    else if (difference > MAX_LEFT_LIMIT && difference < SLIGHT_RIGHT_LIMIT)  
    {  
        geo_cmd.GEOGRAPHICAL_CMD_direction = (1 << GEO_RIGHT);
        dir = 'R';
        // puts(" right");
    }
    else if (difference < MAX_RIGHT_LIMIT && difference > SLIGHT_LEFT_LIMIT) 
    {   
        geo_cmd.GEOGRAPHICAL_CMD_direction = (1 << GEO_LEFT);
        dir = 'L';
         // puts(" left");
    }
    else
    {   
        geo_cmd.GEOGRAPHICAL_CMD_direction = (1 << GEO_STRAIGHT);
        dir = 'F';
        // puts("stright");
    }

    if(e_distance < MIN_DISTANCE_LEFT)
    {
        geo_cmd.GEOGRAPHICAL_CMD_direction = (1 << GEO_STOP);
        dir = 'S';
    }
    if(e_distance < 10)
    {
        LD.setRightDigit((int)e_distance + '0');
    }
    LD.setLeftDigit(dir);
#endif // DEBUG_GEO

    curr_lat_long.GEOGRAPHICAL_CURR_LAT = e_curr_latitude;
    curr_lat_long.GEOGRAPHICAL_CURR_LONG = e_curr_longitude;

    dbc_encode_and_send_GPS_VALUE_CMD(&gps_data);
    dbc_encode_and_send_GEO_ALERT(&geo_alerts);
    dbc_encode_and_send_COMPASS_VALUE_CMD(&heading_value);
#endif // MOCK_GEO

    dbc_encode_and_send_GEOGRAPHICAL_CURR_LAT_LONG(&curr_lat_long);
    dbc_encode_and_send_GEOGRAPHICAL_CMD(&geo_cmd); 

}
void periodic_geographical_100HZ(void)
{
    can_rx();
}

bool geo_bridge_handshake(bool sent_checkpoints_done)
{
    bool done = false;
    bool success = false;
    static geo_bridge_handshake_sm next_state = idle;
    static geo_bridge_handshake_sm curr_state = idle;
     
    curr_state = next_state;

    switch(curr_state)
    {
        case idle:
            if(s_bridge_num_checkpoint != 0)
            {
                free(s_checkpoint_data);
                if(s_checkpoint_data == NULL)
                {    
                    // u0_dbg_printf("num checkpoints: %d\r\n", s_bridge_num_checkpoint);
                    s_num_of_checkpoints = s_bridge_num_checkpoint;
                    s_checkpoint_data = (checkpoint_t*) malloc(s_num_of_checkpoints* sizeof(checkpoint_t));
                    if(s_checkpoint_data == NULL)
                    {
                        // Failed to allocate requested block of memory :(
                        next_state = idle;
                    }
                    else
                    {
                        next_state = geo_receive_lat_long;
                    }
                }
            }
            break;
        case geo_receive_lat_long: 
            if(s_checkpoint_lat_long_count == s_num_of_checkpoints)
            {
                if(sent_checkpoints_done)
                {
                    next_state = geo_num_checkpoint_received; 
                    s_bridge_send_done_long_lat = false;
                }
                else
                {
                    next_state = geo_receive_lat_long;   
                }
            }
            break;
        case geo_num_checkpoint_received: 
            s_checkpoints_left = s_checkpoint_lat_long_count;
            send_checkpoint_received_cmd.GEOGRAPHICAL_RESP_NUM_CMD = s_checkpoint_lat_long_count;
            success = dbc_encode_and_send_GEOGRAPHICAL_RESP_NUM(&send_checkpoint_received_cmd);
            if(success)
            {
                // u0_dbg_printf("sending these: %d\r\n", send_checkpoint_received_cmd.GEOGRAPHICAL_RESP_NUM_CMD);
                s_bridge_num_checkpoint = 0;
                next_state = idle;
            }
            else
            {
                s_bridge_num_checkpoint = 0;
                next_state = idle;
            }
            // for(int i =0; i < s_num_of_checkpoints; i++)
            // {
            //     u0_dbg_printf("array lat:%f long:%f reached:%d\r\n", s_checkpoint_data[i].latitude, s_checkpoint_data[i].longitude, s_checkpoint_data[i].reached_checkpoint);
            // }
            break;
        default: 
            curr_state = idle;
            break;
    }
    return !done;    
}

bool command_car(bool send_stop, bool send_go)
{
    bool success = true;

    char dir = ' ';
    if(send_stop)
    {
        geo_cmd.GEOGRAPHICAL_CMD_direction = (1 << GEO_STOP);
        dir = 'S';
        // u0_dbg_printf("stop sent command car\r\n");
        free(s_checkpoint_data); 
        s_checkpoint_data = NULL; // set freed or unallocated pointer to NULL after free so we do not corrupt memory, free(NULL) is harmless
        s_send_stop_to_master = false;
        s_send_go_to_master = false;

        // reset these flags and counters
        s_checkpoint_lat_long_count = 0;
        s_checkpoints_left = 0;
        // s_bridge_num_checkpoint = 0;
        s_num_of_checkpoints = 0;
        s_closest_checkpoint_index = 0;
        LE.off(1);
    }
    else if(send_go)
    {
        // u0_dbg_printf("start sent commmand car\r\n");
        // for(int i =0; i < s_num_of_checkpoints; i++)
        // {
        //     u0_dbg_printf("lat:%f long:%f reached:%d\r\n", s_checkpoint_data[i].latitude, s_checkpoint_data[i].longitude, s_checkpoint_data[i].reached_checkpoint);
        // }
        // Check if checkpoints are set
        if((s_checkpoint_data != NULL) && (s_num_of_checkpoints == s_checkpoint_lat_long_count))
        {
            LE.on(1);
            if(!s_closest_checkpoint_found)
            {

                if(s_closest_checkpoint_index != s_num_of_checkpoints)
                {
                    // checkpoints still left
                    s_closest_checkpoint_found = true;
                    e_checkpoint_latitude = s_checkpoint_data[s_closest_checkpoint_index].latitude;
                    e_checkpoint_longitude = s_checkpoint_data[s_closest_checkpoint_index].longitude;
                }
                else
                {
                    // all checkpoints reached
                    s_closest_checkpoint_index = 0;
                    s_closest_checkpoint_found = false;
                    s_send_go_to_master = false;
                    s_send_stop_to_master = true;
                    // next_state = bridge_send_stop; // send request stop to stop car
                }

            }
            else
            {
                // next_state = bridge_send_go;

                // closest checkpoint found, steer car towards closest checkpoint
                float difference = e_heading - e_bearing;
                if (difference <= MAX_LEFT_LIMIT) // <= -160
                {
                    geo_cmd.GEOGRAPHICAL_CMD_direction = (1 << GEO_LEFT);
                    // puts("max left");
                    dir = 'L';
                }
                else if (difference >= MAX_RIGHT_LIMIT) // >= 200
                {   
                    geo_cmd.GEOGRAPHICAL_CMD_direction = (1 << GEO_RIGHT);
                    // puts("max right");
                    dir = 'G';
                }
                else if (difference > MAX_LEFT_LIMIT && difference < SLIGHT_RIGHT_LIMIT) //  > -160 &&  < -25
                {   
                    geo_cmd.GEOGRAPHICAL_CMD_direction = (1 << GEO_RIGHT);
                    // puts(" right");
                    dir = 'G';
                }
                else if (difference < MAX_RIGHT_LIMIT && difference > SLIGHT_LEFT_LIMIT) // < 200 && > 15
                {    
                    geo_cmd.GEOGRAPHICAL_CMD_direction = (1 << GEO_LEFT);
                     // puts(" left");
                    dir = 'L';
                }
                else
                {   
                    geo_cmd.GEOGRAPHICAL_CMD_direction = (1 << GEO_STRAIGHT); // -25 to 15
                    // puts("stright");
                    dir = 'F';
                }
                if(e_gps_distance_valid)
                {
                    if(e_distance <= MIN_DISTANCE_LEFT)
                    {
                        // Mark as checkpoint reached, now find next closest checkpoint
                        s_checkpoint_data[s_closest_checkpoint_index].reached_checkpoint = true;
                        s_closest_checkpoint_found = false; // reset
                        s_checkpoints_left--;   
                        s_closest_checkpoint_index++;
                    }
                }
            }
        }
        else
        {   
            // checkpoints not set and go is sent from app
            s_closest_checkpoint_index = 0;
            s_closest_checkpoint_found = false;
            s_send_go_to_master = false;
            success = false;
        }
    }
    LD.setLeftDigit(dir);
    LD.setRightDigit(s_checkpoints_left%10+ '0');
    return success;
}


int find_closest_checkpoint(checkpoint_t *points, int size)
{
    int i=0;
    int j=0;
    float minimum = 0.0;
    int minimum_distance_index = 0;
    float min = 0.0;
    bool min_start_index_found = false;

    // find initial lowest index to start search with
    for(i=0; i < size; i++)
    {
        if(points[i].reached_checkpoint != true)
        {
            minimum_distance_index = i;
            minimum = compute_distance(e_curr_latitude, e_curr_longitude, points[i].latitude, points[i].longitude);
            min_start_index_found = true;
            break;
        }
    }
    if(min_start_index_found)
    {
        j = minimum_distance_index +1;
        for(i=j; i < size; i++)
        {
            if(points[i].reached_checkpoint != true)
            {
                min = compute_distance(e_curr_latitude, e_curr_longitude, points[i].latitude, points[i].longitude);
                if(min < minimum)
                {
                    minimum_distance_index = i;
                    minimum = min;
                }
            }
        }
    }
    else
    {
        // no minimum start index found, therefore all checkpoints reached
        minimum_distance_index = ALL_CHECKPOINT_REACHED;
    }
    return minimum_distance_index;
}

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);

    return CAN_tx(CAN_HANDLE, &can_msg, 0);
}

bool setup_can_filter(void)
{
    bool success = false;

    const can_std_id_t *slist       = NULL;
    // Send ACK for 400 - 500
    const can_std_grp_id_t sglist[] = {{CAN_gen_sid(can1, 400), CAN_gen_sid(can1, 500)}};
    const can_ext_id_t *elist       = NULL;            // We don't use any extended IDs
    const can_ext_grp_id_t *eglist  = NULL;            // We don't use any extended IDs

    success = CAN_setup_filter(slist, 0, sglist, 1, elist, 0, eglist, 0);

    return success;
}

void can_rx(void)
{
    static can_msg_t can_msg_receive = {0};


    while (CAN_rx(CAN_HANDLE, &can_msg_receive, CAN_WAIT_VALUE))
    {
        dbc_msg_hdr_t can_msg_hdr;
        can_msg_hdr.dlc = can_msg_receive.frame_fields.data_len;
        can_msg_hdr.mid = can_msg_receive.msg_id;

        switch(can_msg_hdr.mid)
        {
            case 400: // unconditional stop signal
                dbc_decode_BRIDGE_STOP(&stop_cmd, can_msg_receive.data.bytes, &can_msg_hdr);
                if(stop_cmd.BRIDGE_STOP_CMD == 1)
                { 
                    s_send_stop_to_master = true;
                }
                break;
            case 401: // start signal for request to send over checkpoints from bridge

                break;
            case 402: // geo controller will ack
                // Send Ack to Bridge
                break;
            case 403: // bridge send over n number of checkpoints to geo controller
                dbc_decode_BRIDGE_NUM_CHECKPOINT(&num_checkpoint_cmd, can_msg_receive.data.bytes, &can_msg_hdr);
                if(num_checkpoint_cmd.BRIDGE_NUM_CHECKPOINT_CMD != 0)
                {
                    s_bridge_num_checkpoint = num_checkpoint_cmd.BRIDGE_NUM_CHECKPOINT_CMD;
                }
                break;
            case 404: // bridge send over n number of checkpoint data (lat, long) to geo controller
                dbc_decode_BRIDGE_LAT_LONG(&lat_long_cmd, can_msg_receive.data.bytes, &can_msg_hdr);
                if((int)lat_long_cmd.BRIDGE_LAT != 0 && (int)lat_long_cmd.BRIDGE_LONG != 0)
                {
                    s_checkpoint_data[s_checkpoint_lat_long_count].latitude = lat_long_cmd.BRIDGE_LAT;
                    s_checkpoint_data[s_checkpoint_lat_long_count].longitude = lat_long_cmd.BRIDGE_LONG;
                    s_checkpoint_data[s_checkpoint_lat_long_count].reached_checkpoint = false;
                    s_checkpoint_lat_long_count++;
                }
                break;
            case 405: // bridge send over done signal signaling finish sending checkpoint data
                dbc_decode_BRIDGE_DONE(&done_cmd, can_msg_receive.data.bytes, &can_msg_hdr);
                if(done_cmd.BRIDGE_DONE_CMD == 1)
                {
                    s_bridge_send_done_long_lat = true;
                }
                break;
            case 406: // geo controller responds with n number of checkpoints received
                // Send number of checkpoints received to Bridge
                break;
            case 407: // bridge sends go 
                dbc_decode_BRIDGE_GO(&go_cmd, can_msg_receive.data.bytes, &can_msg_hdr);
                if(go_cmd.BRIDGE_GO_CMD == 1)
                {
                    s_send_go_to_master = true;
                }
                break;
            case 408: // geo controller sending over car's current position
                // Send current GPS location to Bridge
                break;
            default:
                break;
        }
    }

    can_rx_mia_handler();
}


void can_rx_mia_handler(void)
{
    // Here we will take care of MIA
    if (dbc_handle_mia_BRIDGE_STOP(&stop_cmd, 1))
    {
        // LD.setNumber(40); // corresponds to Message ID without the zero
    }

    if(dbc_handle_mia_BRIDGE_NUM_CHECKPOINT(&num_checkpoint_cmd, 1))
    {
        // LD.setNumber(43); 
    }

    if(dbc_handle_mia_BRIDGE_LAT_LONG(&lat_long_cmd, 1))
    {
        // LD.setNumber(44); 
    }

    if(dbc_handle_mia_BRIDGE_DONE(&done_cmd, 1))
    {
        // LD.setNumber(45); 
    }

    if(dbc_handle_mia_BRIDGE_GO(&go_cmd, 1))
    {
        // LD.setNumber(47); 
    }

    return;
}

void periodic_geographical_1KHZ(void)
{
    
}

void geographical_low_priority_init(void)
{

}

void low_priority_geographical_1Hz(unsigned int count)
{

}

#endif // ECU_GEOGRAPHICAL

