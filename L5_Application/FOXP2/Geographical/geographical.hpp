#ifndef GEOGRAPHICAL_HPP_
#define GEOGRAPHICAL_HPP_


void geographical_init(void);
void periodic_geographical_1HZ(void);
void periodic_geographical_10HZ(void);
void periodic_geographical_100HZ(void);
void periodic_geographical_1KHZ(void);

void geographical_low_priority_init(void);
void low_priority_geographical_1Hz(unsigned int);

#endif /* GEOGRAPHICAL_HPP_ */
