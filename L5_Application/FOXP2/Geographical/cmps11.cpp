/*
 * cmps11.cpp
 *
 *  Created on: 2017/11/07
 *      Author: YuYu
 */

#include "cmps11.hpp"
#include <stdio.h>
#include <math.h>
#include <i2c2.hpp>
#include <stdbool.h>
#include "io.hpp" 


#ifdef ECU_GEOGRAPHICAL

#define CMPS11_ADDR 		0xC0
#define SW_VER              0x3
#define CAL_FIRST_SEQ       0xF0
#define CAL_SECOND_SEQ      0xF5
#define CAL_THIRD_SEQ       0xF6
#define CAL_END_SEQ         0xF8

typedef enum 
{
	common_read_write 		= 0,
	heading_8_bit			= 1,
	heading_16_bit_hi		= 2,
	heading_16_bit_lo		= 3,
	pitch_angle				= 4,
	roll_angle				= 5,
	mag_x_raw_hi			= 6,
	mag_x_raw_lo			= 7,
	mag_y_raw_hi			= 8,
	mag_y_raw_lo			= 9,
	mag_z_raw_hi			= 10,
	mag_z_raw_lo			= 11,
	accel_x_raw_hi			= 12,
	accel_x_raw_lo			= 13,
	accel_y_raw_hi			= 14,
	accel_y_raw_lo			= 15,
	accel_z_raw_hi			= 16,
	accel_z_raw_lo			= 17,
	gyro_x_raw_hi			= 18,
	gyro_x_raw_lo 			= 19,
	gyro_y_raw_hi			= 20,
	gyro_y_raw_lo 			= 21,
	gyro_z_raw_hi			= 22,
	gyro_z_raw_lo 			= 23,
	temp_raw_hi				= 24,
	temp_raw_lo				= 25,
	pitch_angle_no_filter	= 26,
	roll_angle_no_filter	= 27
}register_list;

typedef enum 
{
    idle                    = 0,
    first_sequence_0xf0     = 1,
    second_sequence_0xf5    = 2,
    third_sequence_0xf6     = 3,
    end_calibration_0xf8    = 4
}calibrate_state;

static I2C2 *s_compass;

bool cmps11_init(void)
{
	bool success = false;
	s_compass = &(I2C2::getInstance());
	uint8_t sw_version = 0;

    s_compass->readRegisters(CMPS11_ADDR, common_read_write, &sw_version, 1);

    if(sw_version == SW_VER)
    {
        success = true;
    }
    else
    {
        puts("FAILED compass init!!!");
    }

    return success;

}

float cmps11_get_heading(void)
{
    uint16_t heading = 0;
    uint8_t heading_byte[2] = {0};
    s_compass->readRegisters(CMPS11_ADDR, heading_16_bit_hi, &heading_byte[0], 2);
    
    heading = heading_byte[0] << 8 | heading_byte[1];

    return (float)heading/10.0;
}

bool cmps11_calibrate(bool start_calibration, bool end_calibration)
{

    bool done = false;
    static calibrate_state next_state = idle;
    static calibrate_state curr_state = idle;
    
    curr_state = next_state;
    switch(curr_state)
    {
        case idle: 
            if(start_calibration)
            {
                next_state = first_sequence_0xf0;
            }
            else
            {
                next_state = idle;
            }
             break;
        case first_sequence_0xf0:
            s_compass->writeReg(CMPS11_ADDR, common_read_write, CAL_FIRST_SEQ);
            next_state = second_sequence_0xf5;
            break;
        case second_sequence_0xf5:
            s_compass->writeReg(CMPS11_ADDR, common_read_write, CAL_SECOND_SEQ);
            next_state = third_sequence_0xf6;
            break;
        case third_sequence_0xf6:
            s_compass->writeReg(CMPS11_ADDR, common_read_write, CAL_THIRD_SEQ);
            next_state = end_calibration_0xf8;
            break;
        case end_calibration_0xf8:
            if(end_calibration)
            {
                s_compass->writeReg(CMPS11_ADDR, common_read_write, CAL_END_SEQ);
                next_state = idle;
                done = true;
            }
            else
            {
                next_state = end_calibration_0xf8;
            }
            break;
        default:
            next_state = idle;
    }

    return !done;
}

#endif // ENCU_GEOGRAPHICAL