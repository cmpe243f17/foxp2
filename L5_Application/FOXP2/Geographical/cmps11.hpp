/*
 * cmps11.hpp
 *
 *  Created on: 2017/11/07
 *      Author: YuYu
 */

#ifndef L5_APPLICATION_CMPS11_HPP_
#define L5_APPLICATION_CMPS11_HPP_

/*
http://www.robot-electronics.co.uk/htm/cmps11i2c.htm
Library for CMPS11
*/

#ifdef ECU_GEOGRAPHICAL

bool cmps11_init(void);
float cmps11_get_heading(void);
bool cmps11_calibrate(bool start_calibration, bool end_calibration);

#endif // ECU_GEOGRAPHICAL

#endif /* L5_APPLICATION_CMPS11_HPP_ */