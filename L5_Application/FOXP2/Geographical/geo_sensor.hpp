#ifndef GEO_SENSOR_HPP_
#define GEO_SENSOR_HPP_

#ifdef ECU_GEOGRAPHICAL

extern float e_heading;
extern float e_bearing;
extern float e_distance;
extern float e_curr_latitude;
extern float e_curr_longitude;
extern float e_checkpoint_latitude;
extern float e_checkpoint_longitude;

extern uint8_t e_heading_valid;
extern bool e_gps_distance_valid;

bool geo_sensor_init(void);
void geo_sensor_run(void);
float compute_distance(float gps_lat, float gps_long, float dst_lat, float dst_long); // calculate distance

#endif // ECU_GEOGRAPHICAL

#endif // GEO_SENSOR_HPP_