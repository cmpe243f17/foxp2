/*
 * gps.hpp
 *
 *  Created on: 2015�~11��19��
 *      Author: YuYu
 */

#ifndef L5_APPLICATION_GPS_HPP_
#define L5_APPLICATION_GPS_HPP_

#ifdef ECU_GEOGRAPHICAL

typedef void(*gps_data_ready_cb)(void);

bool gps_init(gps_data_ready_cb cb);
bool get_lat_long(float *gps_data); //

#endif // ECU_GEOGRAPHICAL

#endif /* L5_APPLICATION_GPS_HPP_ */
