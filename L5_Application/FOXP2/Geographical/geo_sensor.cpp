#include "FreeRTOS.h"
#include "semphr.h"
#include "hmc5883l.hpp"
#include "gps.hpp"
#include "cmps11.hpp"
#include <stdbool.h>
#include "io.hpp"
#include <stdio.h>
#include <math.h>
#include "geo_sensor.hpp"
#include "gpio.hpp"
#include <printf_lib.h>


#ifdef ECU_GEOGRAPHICAL

/*
    LED indicator

    LE(1) - (On) = go, (Off) = stop from bridge
    LE(2) - if(On) GPS fix (active), else it is not (void)
    LE(3) - Pulse per second pin from GPS
    LE(4) - if(On) Compass is in calibration, else it is not
    PCB LED - if(On) GPS data valid, else it is not

    On-board push buttons
    
    SW(1) - unused if not mocking
    SW(2) - unused if not mocking
    SW(3) - Starts counter for compass calibration, if count is >= 100 calibration starts
    SW(4) - End compass calibration
*/

// Enable these defines when module is connected to board
#define CMPS11              1
#define HMC5883L            0
#define GPS                 1

#define GPS_NOT_FIXED_ERROR 1000000
#define SMA_WINDOW          10
#define EARTH_RADIUS        6371000 //meters

#define MIN_DEGREES         0
#define MAX_DEGREES         360

#define CAL_COUNTER_LIMIT   100
#if CMPS11
static bool s_cmps11_calibration = false;
static bool s_start_cal = false;
static bool s_end_cal = false;
static int s_cmps11_cal_counter = 0;
#endif


#if HMC5883L
SemaphoreHandle_t compass_semaphore;
static void compass_data_ready(void)
{
    xSemaphoreGiveFromISR(compass_semaphore, pdFALSE);
}
#endif // HMC5883L

#if GPS
static GPIO s_gps_fix_led(P2_2);
SemaphoreHandle_t gps_semaphore;
static void gps_fix(void)
{
    xSemaphoreGiveFromISR(gps_semaphore, pdFALSE);
}
#endif // GPS

static float compute_bearing(float gps_lat, float gps_long, float dst_lat, float dst_long); // calculate bearing
static float to_radian(float degree);
static float to_degree(float radian);
static bool distance_simple_moving_average(float distance);

bool geo_sensor_init(void)
{
    bool success = false;
#if HMC5883L
    compass_semaphore = xSemaphoreCreateBinary();
    xSemaphoreTake(compass_semaphore, 0);
#endif

#if GPS
    gps_semaphore = xSemaphoreCreateBinary();
    xSemaphoreTake(gps_semaphore, 0);
    // s_gps_fix_led.enablePullUp();
    s_gps_fix_led.setAsOutput();
    success = gps_init(gps_fix);
#endif

    if(success)
    {
#if CMPS11
        success = cmps11_init();
#elif HMC5883L
        success = hmc_init(compass_data_ready);
#endif // CMPS11
    }

    if(!success)
    {
        puts("GEO SENSOR failed to init!!!!! fix me!!");
    }

    return success;
}


void geo_sensor_run(void)
{
    bool gps_data_valid = false;
    bool lat_long_valid = false;
    bool heading_data_valid = false;
#if CMPS11
    float cmps11_heading = 0;
    if(SW.getSwitch(3))
    {
        s_cmps11_cal_counter++;
    }

    if(s_cmps11_cal_counter >= CAL_COUNTER_LIMIT)
    {
        s_cmps11_calibration = true;
        s_start_cal = true;
        s_end_cal = false;
    }
    if(SW.getSwitch(4))
    {
        s_cmps11_cal_counter = 0;
        s_start_cal = false;
        s_end_cal = true;
    }

    if(s_cmps11_calibration)
    {
        s_cmps11_calibration = cmps11_calibrate(s_start_cal, s_end_cal);
        heading_data_valid = false;
        s_cmps11_cal_counter = 0;
        LE.on(4);
    }
    else
    {
        cmps11_heading = cmps11_get_heading();
        // check heading value is in range of 0 to 360 degrees
        if(cmps11_heading >= MIN_DEGREES && cmps11_heading <= MAX_DEGREES)
        {
            heading_data_valid = true;
            // printf("cmps11: %f\r\n", cmps11_heading);
        }
        else
        {
            heading_data_valid = false;   
        }
        LE.off(4);
    }

#elif HMC5883L
    float heading = 0;
    if(xSemaphoreTake(compass_semaphore, 0) == pdTRUE)
    {
        heading = hmc_get_heading();
        heading_data_valid = true;
        // printf("hmc: %f\r\n", heading);
    }
    else
    {
        heading_data_valid = false;
    }

#endif // CMPS11

#if GPS
    if(xSemaphoreTake(gps_semaphore, 0) == pdTRUE)
    {
        // s_gps_fix_led.setHigh();
        LE.on(3);
    }
    else
    {
        LE.off(3);
        // s_gps_fix_led.setLow();
    }

    float gps_data[2] = {0.0};  // [0] = current latitude , [1] = current longitude

    lat_long_valid = get_lat_long(gps_data);
    float curr_latitude = gps_data[0];
    float curr_longitude = gps_data[1];

    float bearing = compute_bearing(curr_latitude, curr_longitude, e_checkpoint_latitude, e_checkpoint_longitude);
    float distance =  compute_distance(curr_latitude, curr_longitude, e_checkpoint_latitude, e_checkpoint_longitude);

    gps_data_valid = distance_simple_moving_average(distance);

    if(lat_long_valid)
    {
        LE.on(2);
    }
    else
    {
        LE.off(2);
    }
    e_gps_distance_valid = gps_data_valid;
    // u0_dbg_printf("lat: %f long:%f\r\n", curr_latitude, curr_longitude);
    if(lat_long_valid && gps_data_valid)
    {   
        e_bearing = bearing;
        e_distance = distance;
        e_curr_latitude = curr_latitude;
        e_curr_longitude = curr_longitude;
        s_gps_fix_led.setHigh();
        // printf("d: %f  sma: %f\r\n", distance, distance_sma);
    }
    else
    {
        s_gps_fix_led.setLow();
    }
#endif // GPS 

#if (HMC5883L || CMPS11)
    if(heading_data_valid)
    {
#if CMPS11
         // printf("h: %f b: %f\r\n" , cmps11_heading, bearing);
        e_heading = cmps11_heading;
        e_heading_valid = 1;
#elif HMC5883L
         // printf("h: %f b: %f\r\n" , heading, bearing);
        e_heading = heading;
#endif // CMPS11
    }
    else
    {
        e_heading_valid = 0;
    }
#endif // (HMC5883L || CMPS11)
}


float compute_bearing(float gps_lat, float gps_long, float dst_lat, float dst_long)
{
    // bearing = atan2( sin(longDiff_rad)*cos(lat2_rad),
    //cos(lat1_rad)*sin(lat2_rad)-sin(lat1_rad)*cos(lat2_rad)*cos(longDiff_rad))

    float lat1_rad = to_radian(gps_lat);
    float lat2_rad = to_radian(dst_lat);
    float longDiff_rad = to_radian(dst_long - gps_long);

    float y = sin(longDiff_rad) * cos(lat2_rad);
    float x = cos(lat1_rad) * sin(lat2_rad)
            - sin(lat1_rad) * cos(lat2_rad) * cos(longDiff_rad);
    float bearing = fmodf(to_degree(atan2(y, x)) + 360, 360);

    return bearing;
}

float compute_distance(float gps_lat, float gps_long, float dst_lat, float dst_long)
{
    //haversine formula: a = sin^2(latDiff_rad/2) + cos(lat1_rad) * cos(lat2_rad) * sin^2(longDiff_rad/2)
    // formula: c = 2 *atan2(sqrt(a), sqrt(1-a))
    // d = R*c

    float earth_radius = EARTH_RADIUS;
    float lat1_rad = to_radian(gps_lat);
    float lat2_rad = to_radian(dst_lat);
    float latDiff_rad = to_radian(dst_lat - gps_lat);
    float longDiff_rad = to_radian(dst_long - gps_long);

    float a = sin(latDiff_rad / 2) * sin(latDiff_rad / 2)
            + cos(lat1_rad) * cos(lat2_rad) * sin(longDiff_rad / 2)
                    * sin(longDiff_rad / 2);

    float c = 2.0 * atan2(sqrt(a), sqrt(1 - a));
    float d = earth_radius * c;

    return d;
}

bool distance_simple_moving_average(float distance)
{
    bool success = false;
    static float distance_moving_avg[SMA_WINDOW] = {0};
    static float running_count = 0;
    static int sma_count = 0;
    static float distance_sma = 0;

    // Parsing error
    if(distance > GPS_NOT_FIXED_ERROR)
    {
        success = false;
    }
    else
    {
        // Simple running average (SMA) of distance values to filter bad parsing
        // Build up initial value
        if(sma_count < SMA_WINDOW)
        {
            distance_moving_avg[sma_count] = distance;
            running_count += distance_moving_avg[sma_count++];
        }
        else
        {
            // printf("sma: %f\r\n", distance_sma);

            running_count += distance;
            running_count -= distance_moving_avg[sma_count % SMA_WINDOW];
            distance_moving_avg[sma_count++ % SMA_WINDOW] = distance;

        }
        // Calculate SMA based on last 10 values
        if(sma_count% SMA_WINDOW == 0)
        {
            distance_sma = running_count/SMA_WINDOW;
        }

        // If current distance is less than or equal to twice the SMA, it is valid
        if(distance <= distance_sma*2)
        {
            success = true;
        }
    }
    return success;
}

float to_radian(float degree)
{
    return degree * (M_PI / 180.0);
}

float to_degree(float radian)
{
    return radian * (180.0 / M_PI);
}

#endif // ECU_GEOGRAPHICAL