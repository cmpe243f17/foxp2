/*
 * gps.cpp
 *
 *  Created on: 2015�~11��19��
 *      Author: YuYu
 */

#include "gps.hpp"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "io.hpp"
#include "uart3.hpp"
#include "eint.h"
#include <printf_lib.h>

#ifdef ECU_GEOGRAPHICAL

// Debugs
#define DEBUG       0
#define DEBUG_1     0
#define DEBUG_2     0

// Uart3
#define BAUD_RATE_9600      9600
#define BAUD_RATE_57600     57600
#define RX_SIZE             1000
#define TX_SIZE             1000

// GPS
#define MINRANGE_LONGITUDE  -120    // range of location in bay area, california
#define MAXRANGE_LONGITUDE  -123
#define MINRANGE_LATITUDE   36
#define MAXRANGE_LATITUDE   38
#define MAX_LONGITUDE       180
#define MAX_LATITUDE        90
#define DELIMITER           ","
#define PMTK_SET_NMEA_OUTPUT_RMCONLY "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29"
#define PMTK_SET_NMEA_UPDATE_10HZ "$PMTK220,100*2F"
#define PMTK_SET_NMEA_UPDATE_5HZ  "$PMTK220,200*2C"
#define PMTK_SET_NMEA_UPDATE_2HZ  "$PMTK220,500*2B"
#define PMTK_SET_BAUD_57600 "$PMTK251,57600*2C"
#define PMTK_SET_NMEA_UPDATE_200_MILLIHERTZ  "$PMTK220,5000*1B"
#define PMTK_API_SET_FIX_CTL_200_MILLIHERTZ  "$PMTK300,5000,0,0,0,0*18"


static Uart3 *s_gps;

static gps_data_ready_cb s_gps_fix_ready = 0;

static float s_latitude = 37; // for gps module, default values
static float s_longitude = -121; // for gps module, default values


static bool tokenize(char *line);  // parse RMC sentence for latitude and longitude
static float convert_nmea_sentence_to_decimal_coord(float coordinates, const char *val); // self-explanatory

static void gps_fix_cb(void)
{
    if(s_gps_fix_ready)
    {
        s_gps_fix_ready();
    }
}

bool gps_init(gps_data_ready_cb cb)
{
    bool success = false;
    if(!cb)
    {
        success = false;
    }
    else
    {
        s_gps_fix_ready = cb;
        // data ready when interrupt generated is low, P2.1
        eint3_enable_port2(1 , eint_rising_edge, (void_func_t)gps_fix_cb);

        s_gps = &(Uart3::getInstance());

        success = s_gps->init(BAUD_RATE_9600, RX_SIZE, TX_SIZE); // Baud rate, rx Queue size, tx Queue size

        if(success)
        {
            // update fix rate to 5 seconds
            s_gps->putline(PMTK_SET_NMEA_UPDATE_200_MILLIHERTZ, portMAX_DELAY);
            s_gps->putline(PMTK_API_SET_FIX_CTL_200_MILLIHERTZ, portMAX_DELAY);
            // Output only $GPRMC sentences
            s_gps->putline(PMTK_SET_NMEA_OUTPUT_RMCONLY, portMAX_DELAY);
            // update 10Hz
            s_gps->putline(PMTK_SET_NMEA_UPDATE_10HZ, portMAX_DELAY);
            // set baudrate
            s_gps->putline(PMTK_SET_BAUD_57600, portMAX_DELAY);
            success = s_gps->init(BAUD_RATE_57600, RX_SIZE, TX_SIZE); // Baud rate, rx Queue size, tx Queue size
        }
        else
        {
            puts("UART3 driver init failed!!!!!!!!!!! FIX ME");
        }
    }
    return success;
}

bool get_lat_long(float *gps_data)
{
    // recieve buffer, upper half is parsing NMEA RMC for latitude and longitude
    char rx[80] ={'\0'};
    bool active = false;
    active = s_gps->gets(rx, 80, 0);
#if DEBUG
   printf("%s\r\n", rx);
#endif
    if (active && rx != NULL && strstr(rx, "$GPRMC")) // look for $GPRMC in string
    {
        active = tokenize(rx); // split into tokens (parsing)
#if DEBUG_1 || DEBUG
        printf("\nlat: %f      long: %f\r\n", s_latitude, s_longitude);
#endif
    }
    if(active)
    {
        gps_data[0] = s_latitude;
        gps_data[1] = s_longitude;
    }
    return active;
}


bool tokenize(char *line)
{
    const char delimiter[2] = DELIMITER;
    char *token;
    int counter = 0;
    bool active_or_void = false;
    // token= strtok(line, delimiter);
    float latTemp = s_latitude;
    float longTemp = s_longitude;
    while((token = strtok_r(line, delimiter, &line)))
    // while(token)
    {
#if DEBUG_2
        // u0_dbg_printf("counter : %d   token: %s\r\n", counter, token);
#endif
        if (counter == 2)
        {
            // active or void
            if(*token == 'A')
            {
                active_or_void = true;
            }
        }
        if (counter == 3)
        {/*convert the 3trd token latitude*/
            latTemp = atof(token);
            latTemp = convert_nmea_sentence_to_decimal_coord(latTemp, "m"); //"m" for meridian
        }
        /*If 5th token == South multiply by -1*/
        if (counter == 4)
        {
            if (*token == 'S')
            {
                latTemp *= (-1);
            }
        }
        s_latitude = latTemp;
        /*convert the 5th token longitude*/
        if (counter == 5)
        {
            longTemp = atof(token);
            longTemp = convert_nmea_sentence_to_decimal_coord(longTemp, "p"); //"p" for parallel
        }
        /*If 6th token == West multiply by -1*/
        if (counter == 6)
        {
            if (*token == 'W')
            {
                longTemp *= (-1);
            }
        }
        s_longitude = longTemp;
        // token = strtok(NULL, delimiter);
        ++counter;
        if(counter >=7)
        {
            break;
        }
    }
    return active_or_void;
}

float convert_nmea_sentence_to_decimal_coord(float coordinates, const char *val)
{
    /* Sample from gps 5153.6605*/
    /* Check limits*/
    if ((*val == 'm') && (coordinates < 0.0 && coordinates > MAX_LATITUDE))
    {
        return 0;
    }
    if (*val == 'p' && (coordinates < 0.0 && coordinates > MAX_LONGITUDE))
    {
        return 0;
    }
    int b; //to store the degrees
    float c; //to store the decimal

    /*Calculate the value in format nn.nnnnnn*/
    /*Explanations at:
     http://www.mapwindow.org/phorum/read.php?3,16271,16310*/

    b = coordinates / 100; // 51 degrees
    c = (coordinates / 100 - b) * 100; //(51.536605 - 51)* 100 = 53.6605
    c /= 60; // 53.6605 / 60 = 0.8943417
    c += b; // 0.8943417 + 51 = 51.8943417
    return c;
}

#endif // ECU_GEOGRAPHICAL