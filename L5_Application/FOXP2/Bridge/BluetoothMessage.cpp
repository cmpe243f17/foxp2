/*
 * BluetoothMessage.cpp
 *
 *  Created on: Nov 25, 2017
 *      Author: vin
 */

#include "BluetoothMessage.hpp"

void BluetoothMessage::setPayload(LatLng *currentPayload, uint8_t size) {
	if (this->payload != NULL) {
		delete [] this->payload;
		this->payload = NULL;
	}
	this->payload = new LatLng[size];
	this->payloadSize = size;
	for (int i = 0; i < size; i++) {
		this->payload[i] = currentPayload[i];
	}
	this->signal = CHECKPOINT_SIGNAL;
}

/**
 * put this payload to currentPayload.
 */
uint8_t BluetoothMessage::readPayload(LatLng* currentPayload) {
	for (int i = 0; i < this->payloadSize; i++) {
		currentPayload[i] = this->payload[i];
	}
	resetSignal(); //reset signal once payload is read.
	return this->payloadSize;
}
