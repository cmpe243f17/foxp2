#include "generated_can.h"
#include "bridge.hpp"
#include <stdio.h>
#include "can.h"
#include "Build_Config.hpp"
#include "io.hpp"
#include <string.h>
#include "printf_lib.h"
#include "uart2.hpp"

#ifdef ECU_BRIDGE
#include "BluetoothMessage.hpp"

#define CAN_HANDLE              can1
#define DEFAULT_MIA_MS          1000
#define GEO_RESPONSE_TIMEOUT    20

typedef enum CheckpointStates {
  idle                              =   0,
  sendingNumberOfCheckpoints        =   1,
  sendingCheckpoints                =   2,
  sendingCheckpointsDone            =   3,
  waitingGeoResponse                =   4
} CheckpointStates_t;

typedef enum SendToAppStates {
  sendingHearbeat                =      0,
  sendingSensorStatus            =      1,
  sendingGeoCurrentLocation      =      2
} SendToAppStates_t;

// Globals
const uint32_t                             GEOGRAPHICAL_ACK__MIA_MS = DEFAULT_MIA_MS;
const uint32_t                             GEOGRAPHICAL_RESP_NUM__MIA_MS =DEFAULT_MIA_MS;
const GEOGRAPHICAL_RESP_NUM_t              GEOGRAPHICAL_RESP_NUM__MIA_MSG = {0};
const uint32_t                             GEOGRAPHICAL_CURR_LAT_LONG__MIA_MS = DEFAULT_MIA_MS;
const GEOGRAPHICAL_CURR_LAT_LONG_t         GEOGRAPHICAL_CURR_LAT_LONG__MIA_MSG = {0};



// handshake global variables
static uint8_t s_geo_num_checkpoint_response = 0;


// extern variables, declare these extern where bluetooth handler is
bool e_request_start_handshake = false;
uint8_t e_num_of_checkpoints = 0;
bool e_coordinates_received_from_app = false;
float e_checkpoint_latitude;
float e_checkpoint_longitude;
bool e_app_send_go = false;
bool e_app_send_stop = false;

// values from GEO
float gps_latitude;
float gps_longitude;
bool isNewGPSReadings = false;

// values from SENSOR
uint16_t leftSensorValue;
uint16_t frontSensorValue;
uint16_t rightSensorValue;
bool isNewSensorReadings = false;

// transmitting these
static BRIDGE_STOP_t bridge_stop = {0};
static BRIDGE_NUM_CHECKPOINT_t bridge_num_of_checkpoint_to_send = {0};
static BRIDGE_LAT_LONG_t bridge_checkpoint_lat_long_data = {0};
static BRIDGE_DONE_t bridge_done_sending_checkpoints = {0};
static BRIDGE_GO_t bridge_go = {0};


// receiving these
static GEOGRAPHICAL_RESP_NUM_t geo_response_checkpoint_num = {0};
static GEOGRAPHICAL_CURR_LAT_LONG_t geo_location = {0};
static SENSOR_PROX_STATUS_t sensorStatus = {0};

void sendCurrentGeoLocation();
void sendSensorStatus();
void sendGoSignal();
void sendStopSignal();
void sendNumberOfCheckpoints(uint8_t numberOfCheckpoints);
void sendCheckpoint(LatLng payload);
void sendDoneSendingCheckpoints();

static bool setup_can_filter(void);
static void can_rx(void);
static void bridge_mia_handler(void);

static char HEARTBEAT_MESSAGE_TO_APP[] = "bridge heartbeat";

BluetoothMessage bluetoothMessage;
volatile bool isNewData = false;


void bridge_init(void)
{
    bool success = false;

    success = CAN_init(CAN_HANDLE, CAN_BAUDRATE, RECEIVER_QUEUE_SIZE, SENDER_QUEUE_SIZE, NULL, NULL);

    if(success)
    {
        // As of now, geographical unit will accept any message, later on filter will be added if needed
        success = setup_can_filter();

        if(success)
        {
            // Do nothing
        }
        else
        {
            puts("Failed to setup CAN filter");
        }

        // Make node (ECU) active on CAN bus by sending reset
        CAN_reset_bus(CAN_HANDLE);
    }
    else
    {
        puts("CAN driver init failed!!!!!!!!!!! FIX ME");
    }
}

void sendMessageToApp(char* msg) {
	Uart2::getInstance().putline(msg);
}

void bridge_low_priority_init() {

}

void low_priority_bridge_1Hz(unsigned int counter) {

}


void periodic_bridge_1HZ(void)
{
    if (CAN_is_bus_off(CAN_HANDLE))
    {
        CAN_reset_bus(CAN_HANDLE);
    }

    static SendToAppStates_t currentState = sendingHearbeat;

    switch (currentState) {
      case sendingHearbeat: {
        sendMessageToApp(HEARTBEAT_MESSAGE_TO_APP);
        currentState = sendingSensorStatus;
        break;
      }
      case sendingSensorStatus: {
        if (isNewSensorReadings) {
          u0_dbg_printf("Received sensor value: %d %d %d\n", leftSensorValue,
                frontSensorValue,
                rightSensorValue);
          sendSensorStatus();
          isNewSensorReadings = false;
        }
        currentState = sendingGeoCurrentLocation;
        break;
      }
      case sendingGeoCurrentLocation: {
        if (isNewGPSReadings) {
          sendCurrentGeoLocation();
          isNewGPSReadings = false;
        }
        currentState = sendingHearbeat;
        break;
      }
      default: break;
    }

    LE.toggle(1);
    return;
}

void periodic_bridge_10HZ(void)
{
    int currentSignal = bluetoothMessage.readSignal();
    static LatLng *payload = NULL;
    static uint8_t payloadSize = 0;
    char messageToApp[50];
    static CheckpointStates_t curr_state = idle;

    if (GO_SIGNAL == currentSignal) {
        u0_dbg_printf("Received GO signal.\n");
    		sendGoSignal();
    } else if (STOP_SIGNAL == currentSignal) {
        u0_dbg_printf("Received STOP signal.\n");
    		sendStopSignal();
    } else if (CHECKPOINT_SIGNAL == currentSignal) {
    		u0_dbg_printf("Received Checkpoints successfully.\n");
    		u0_dbg_printf("Number of checkpoints: %i\n", bluetoothMessage.getPayloadSize());
    		sprintf(messageToApp, "bridge data num_checkpoints %d", bluetoothMessage.getPayloadSize());
    		sendMessageToApp(messageToApp);
    		if (NULL != payload) {
    			delete [] payload;
    			payload = NULL;
    			payloadSize = 0;
    		}
    		payload = new LatLng[bluetoothMessage.getPayloadSize()];
    		payloadSize = (uint8_t)bluetoothMessage.readPayload(payload);
    		isNewData = true;

        // send stop signal for new checkpoints
        sendStopSignal();
        curr_state = sendingNumberOfCheckpoints; // trigger state machine
    }


    static uint8_t payloadIndex = 0;
    static uint8_t timeoutGeoResponse = GEO_RESPONSE_TIMEOUT;

    if (curr_state != idle) {
      switch (curr_state) {
        case sendingNumberOfCheckpoints: {
          sendNumberOfCheckpoints(payloadSize);
          curr_state = sendingCheckpoints;
          break;
        }
        case sendingCheckpoints: {
          if (payloadIndex == payloadSize) {
            curr_state = sendingCheckpointsDone;
            payloadIndex = 0;
          } else {
            sendCheckpoint(payload[payloadIndex]);
            payloadIndex++;
          }
          break;
        }
        case sendingCheckpointsDone: {
          sendDoneSendingCheckpoints();
          curr_state = waitingGeoResponse;
          break;
        }
        case waitingGeoResponse: {
          if (s_geo_num_checkpoint_response != 0) {
            u0_dbg_printf("Received response from GEO: Number of checkpoints: %i\n", s_geo_num_checkpoint_response);
            timeoutGeoResponse = GEO_RESPONSE_TIMEOUT;
            curr_state = idle;
            s_geo_num_checkpoint_response = 0;
            break;
          } else if (0 == timeoutGeoResponse) {
            u0_dbg_printf("Failed to receive response from GEO.\n");
            sendMessageToApp((char*)"bridge msg failed_checkpoints_response_geo");
            timeoutGeoResponse = GEO_RESPONSE_TIMEOUT;
            curr_state = idle;
            break;
          }
          timeoutGeoResponse--;
          break;
        }
        default: {
          u0_dbg_printf("Invalid state!\n");
          break;
        }
      }
    }
}

void sendCurrentGeoLocation() {
  char messageToApp[50];
  sprintf(messageToApp, "geo location %f %f", gps_latitude, gps_longitude);
  sendMessageToApp(messageToApp);
}

void sendSensorStatus() {
  char messageToApp[50];
  sprintf(messageToApp, "sensor status %d %d %d",
          leftSensorValue,
          frontSensorValue,
          rightSensorValue);
  sendMessageToApp(messageToApp);
}

void sendGoSignal() {
  sendMessageToApp((char *)"bridge msg received_go_signal");
  bridge_go.BRIDGE_GO_CMD = 1;
  dbc_encode_and_send_BRIDGE_GO(&bridge_go);
}

void sendStopSignal() {
  sendMessageToApp((char *)"bridge msg received_stop_signal");
  bridge_stop.BRIDGE_STOP_CMD = 1;
  dbc_encode_and_send_BRIDGE_STOP(&bridge_stop);
}

void sendNumberOfCheckpoints(uint8_t numberOfCheckpoints) {
  u0_dbg_printf("Sending number of checkpoints: %i\n", numberOfCheckpoints);
  bridge_num_of_checkpoint_to_send.BRIDGE_NUM_CHECKPOINT_CMD = numberOfCheckpoints;
  dbc_encode_and_send_BRIDGE_NUM_CHECKPOINT(&bridge_num_of_checkpoint_to_send);
}

void sendCheckpoint(LatLng payload) {
  float latitude = payload.getLatitude();
  float longitude = payload.getLongitude();
  u0_dbg_printf("Sending checkpoint: %f %f\n", latitude, longitude);
  bridge_checkpoint_lat_long_data.BRIDGE_LAT = latitude;
  bridge_checkpoint_lat_long_data.BRIDGE_LONG = longitude;
  dbc_encode_and_send_BRIDGE_LAT_LONG(&bridge_checkpoint_lat_long_data);
}

void sendDoneSendingCheckpoints() {
  bridge_done_sending_checkpoints.BRIDGE_DONE_CMD = 1;
  dbc_encode_and_send_BRIDGE_DONE(&bridge_done_sending_checkpoints);
}

void periodic_bridge_100HZ(void)
{
    can_rx();
    return;
}

void periodic_bridge_1KHZ(void)
{
    return;
}

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    static can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);

    return CAN_tx(can1, &can_msg, 0);
}

bool setup_can_filter(void)
{
    bool success = false;

    const can_std_id_t *slist       = NULL;
    // Send ACK for 400 - 500
    const can_std_grp_id_t sglist[] = {
            CAN_gen_sid(can1, 300),
            CAN_gen_sid(can1, 400),
            CAN_gen_sid(can1, 406),
            CAN_gen_sid(can1, 408),
            CAN_gen_sid(can1, 500),
            CAN_gen_sid(can1, 600)};
    const can_ext_id_t *elist       = NULL;            // We don't use any extended IDs
    const can_ext_grp_id_t *eglist  = NULL;            // We don't use any extended IDs

    success = CAN_setup_filter(slist, 0, sglist, 6, elist, 0, eglist, 0);

    return success;
}

void can_rx(void)
{
    can_msg_t can_msg = {0};
    while (CAN_rx(CAN_HANDLE, &can_msg, CAN_WAIT_VALUE))
    {
        dbc_msg_hdr_t can_msg_hdr;
        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_msg.msg_id;

        switch(can_msg_hdr.mid)
        {
            case 300: {
              dbc_decode_SENSOR_PROX_STATUS(&sensorStatus, can_msg.data.bytes, &can_msg_hdr);
              static uint8_t sensorReceiveCounter = 0;
              if ((sensorReceiveCounter % 10) == 0) {
                leftSensorValue = sensorStatus.SENSOR_LFRONT_DIST;
                frontSensorValue = sensorStatus.SENSOR_FRONT_DIST;
                rightSensorValue = sensorStatus.SENSOR_RFRONT_DIST;
                isNewSensorReadings = true;
                sensorReceiveCounter = 0;
              }
              sensorReceiveCounter++;
              break;
            }
            case 400:
                break;
            case 401:
                break;
            case 402:
                break;
            case 403:
                break;
            case 404:
                break;
            case 405:
                break;
            case 406:
                dbc_decode_GEOGRAPHICAL_RESP_NUM(&geo_response_checkpoint_num, can_msg.data.bytes, &can_msg_hdr);
                s_geo_num_checkpoint_response = geo_response_checkpoint_num.GEOGRAPHICAL_RESP_NUM_CMD;
                u0_dbg_printf("Received number of checkpoints from GEO: %i\n", s_geo_num_checkpoint_response);
                break;
            case 407:
                break;
            case 408: {
              static uint8_t gpsReceiveLatLngCounter = 0;
              dbc_decode_GEOGRAPHICAL_CURR_LAT_LONG(&geo_location, can_msg.data.bytes, &can_msg_hdr);
              if ((gpsReceiveLatLngCounter % 10) == 0) {
                gps_latitude = geo_location.GEOGRAPHICAL_CURR_LAT;
                gps_longitude = geo_location.GEOGRAPHICAL_CURR_LONG;
                isNewGPSReadings = true;
                gpsReceiveLatLngCounter = 0;
                u0_dbg_printf("Received LatLng from GEO: %f %f\n", gps_latitude, gps_longitude);
              }
              gpsReceiveLatLngCounter++;
              break;
            }
            default:
                break;
        }
    }
    bridge_mia_handler();
}

void bridge_mia_handler(void)
{
    if(dbc_handle_mia_GEOGRAPHICAL_RESP_NUM(&geo_response_checkpoint_num, 10))
    {
        // Do something
        LD.setNumber(51);
    }
    if(dbc_handle_mia_GEOGRAPHICAL_CURR_LAT_LONG(&geo_location, 10))
    {
        // Do something
        LD.setNumber(52);
    }
}

#endif // ECU_BRIDGE
