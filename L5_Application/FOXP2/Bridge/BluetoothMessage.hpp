/*
 * BluetoothMessage.hpp
 *
 *  Created on: Nov 25, 2017
 *      Author: vin
 */

#ifndef L5_APPLICATION_FOXP2_BRIDGE_BLUETOOTHMESSAGE_HPP_
#define L5_APPLICATION_FOXP2_BRIDGE_BLUETOOTHMESSAGE_HPP_

#define IDLE_SIGNAL 0
#define STOP_SIGNAL 1
#define GO_SIGNAL 2
#define CHECKPOINT_SIGNAL 3


#include "stdio.h"
#include "LatLng.hpp"
#include "printf_lib.h"

class BluetoothMessage {

private:
	int signal = IDLE_SIGNAL;
	uint8_t payloadSize = 0;
  LatLng *payload = NULL;

	void resetSignal() {
		this->signal = IDLE_SIGNAL;
	}
public:
	void setGoSignal() {
		this->signal = GO_SIGNAL;
	}

	void setStopSignal() {
		this->signal = STOP_SIGNAL;
	}

	int readSignal() {
		int currentSignal = this->signal;
		resetSignal();
		return currentSignal;
	}

	int getPayloadSize() {
		return this->payloadSize;
	}

	void setPayload(LatLng payload[], uint8_t size);

	/**
	 * @param payloads - put the current payloads to this pointer.
	 * @return - number of payloads
	 */
	uint8_t readPayload(LatLng* payloads);

	void printPayload() {
		if (this->payload != NULL) {
			for (int i = 0; i < payloadSize; i++) {
				u0_dbg_printf("CHECKPOINT: [%f] [%f]\n", payload[i].getLatitude(), payload[i].getLongitude());
			}
		}
	}
};



#endif /* L5_APPLICATION_FOXP2_BRIDGE_BLUETOOTHMESSAGE_HPP_ */
