/*
 * LatLng.hpp
 *
 *  Created on: Nov 25, 2017
 *      Author: vin
 */

#ifndef L5_APPLICATION_FOXP2_BRIDGE_LATLNG_HPP_
#define L5_APPLICATION_FOXP2_BRIDGE_LATLNG_HPP_

/**
 * Class that represents Latitude and Longitude
 */
class LatLng {

private:
	float latitude = -1;
	float longitude = -1;

public:
	void setLatitude(float newLatitude) {
		this->latitude = newLatitude;
	}
	float getLatitude() {
		return this->latitude;
	}
	void setLongitude(float newLongitude) {
		this->longitude = newLongitude;
	}
	float getLongitude() {
		return this->longitude;
	}
};


#endif /* L5_APPLICATION_FOXP2_BRIDGE_LATLNG_HPP_ */
