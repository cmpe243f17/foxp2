#ifndef BRIDGE_HPP_
#define BRIDGE_HPP_


void bridge_init(void);
void periodic_bridge_1HZ(void);
void periodic_bridge_10HZ(void);
void periodic_bridge_100HZ(void);
void periodic_bridge_1KHZ(void);
void bridge_low_priority_init();
void low_priority_bridge_1Hz(unsigned int counter);


#endif /* BRIDGE_HPP_ */
