/*
 * low_priority_task.h
 *
 *  Created on: Nov 25, 2017
 *      Author: Taylor
 */

#ifndef L5_APPLICATION_FOXP2_LOW_PRIORITY_TASK_HPP_
#define L5_APPLICATION_FOXP2_LOW_PRIORITY_TASK_HPP_

#include "tasks.hpp"
#include "Periodic_Dispatcher.hpp"



class low_priority_task : public scheduler_task
{
    public:
	low_priority_task() :
            scheduler_task("Low Priority", 1024, PRIORITY_LOW)
        {
			LOW_PRIORITY_INIT();
        }

        bool run(void *p)
        {
        	// Doesn't have to be a hard 1s, and avoids timing semaphore dependencies from 1Hz periodic
        	vTaskDelay(1000);

        	// Counter that indicates the rough num of seconds lp task has been running (should go into telemetry later)
        	static unsigned int count = 0;
        	count++;

        	LOW_PRIORITY_1HZ(count);

            return true;
        }
};


#endif /* L5_APPLICATION_FOXP2_LOW_PRIORITY_TASK_HPP_ */
