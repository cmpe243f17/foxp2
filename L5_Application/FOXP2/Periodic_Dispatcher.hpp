/*
 * Periodic_Dispatcher.hpp
 *
 *  Created on: Oct 11, 2017
 *      Author: Ahsan Uddin
 *
 *  This dispatcher will call in the module specific periodic callbacks
 *
 */

#include "generated_can.h"
#include "bridge.hpp"
#include "Motor_Driver.hpp"
#include "periodic_gateway.hpp"
#include "geographical.hpp"
#include "sensor.hpp"
#include "bridge.hpp"

#if defined(ECU_MOTOR)

#define LOW_PRIORITY_INIT() motor_low_priority_init()
#define LOW_PRIORITY_1HZ(x) low_priority_motor_1Hz(x)

#define PERIODIC_INIT()     motor_init()
#define PERIODIC_1HZ()      periodic_motor_1HZ()
#define PERIODIC_10HZ()     periodic_motor_10HZ()
#define PERIODIC_100HZ()    periodic_motor_100HZ()
#define PERIODIC_1KHZ()     periodic_motor_1KHZ()

#elif defined(ECU_SENSOR)

#define LOW_PRIORITY_INIT() sensor_low_priority_init()
#define LOW_PRIORITY_1HZ(x) low_priority_sensor_1Hz(x)

#define PERIODIC_INIT()     sensor_init()
#define PERIODIC_1HZ()      periodic_sensor_1HZ()
#define PERIODIC_10HZ()     periodic_sensor_10HZ()
#define PERIODIC_100HZ()    periodic_sensor_100HZ()
#define PERIODIC_1KHZ()     periodic_sensor_1KHZ()

#elif defined(ECU_GATEWAY)

#define LOW_PRIORITY_INIT() gateway_low_priority_init()
#define LOW_PRIORITY_1HZ(x) low_priority_gateway_1Hz(x)

#define PERIODIC_INIT()     gateway_init()
#define PERIODIC_1HZ()      periodic_gateway_1HZ()
#define PERIODIC_10HZ()     periodic_gateway_10HZ()
#define PERIODIC_100HZ()    periodic_gateway_100HZ()
#define PERIODIC_1KHZ()     periodic_gateway_1KHZ()

#elif defined(ECU_GEOGRAPHICAL)

#define LOW_PRIORITY_INIT() geographical_low_priority_init()
#define LOW_PRIORITY_1HZ(x) low_priority_geographical_1Hz(x)

#define PERIODIC_INIT()     geographical_init()
#define PERIODIC_1HZ()      periodic_geographical_1HZ()
#define PERIODIC_10HZ()     periodic_geographical_10HZ()
#define PERIODIC_100HZ()    periodic_geographical_100HZ()
#define PERIODIC_1KHZ()     periodic_geographical_1KHZ()

#elif defined(ECU_BRIDGE)

#define PERIODIC_INIT()     bridge_init()
#define PERIODIC_1HZ()      periodic_bridge_1HZ()
#define PERIODIC_10HZ()     periodic_bridge_10HZ()
#define PERIODIC_100HZ()    periodic_bridge_100HZ()
#define PERIODIC_1KHZ() 	//periodic_bridge_1KHZ()

#define LOW_PRIORITY_INIT() bridge_low_priority_init()
#define LOW_PRIORITY_1HZ(x) low_priority_bridge_1Hz(x)

#else
#error  Bro, can you please select one of the module??
#endif
