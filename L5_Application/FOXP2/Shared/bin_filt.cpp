#include "bin_filt.hpp"

#include <stdlib.h>

#define MAX(x, y) ((x > y) ? x : y)
#define MIN(x, y) ((x < y) ? x : y)


BinFilt::BinFilt(uint16_t bin_size)
{
    this->bin = (int32_t *)calloc(bin_size, sizeof(int32_t));
    this->size = bin_size;
    this->length = 0U;
    this->wr_idx = 0U;
    this->rd_idx = 0U;
}


void BinFilt::insert(int32_t value)
{
    this->bin[this->wr_idx] = value;
    this->wr_idx_inc();
    this->length = MIN((this->length + 1), this->size);
}


int32_t BinFilt::get_max(void) {
    int32_t ret = INT32_MIN;
    for (uint32_t idx=0U; idx<this->length; idx++) {
        ret = MAX(ret, this->bin[idx]);
    }
    return ret;
}


int32_t BinFilt::get_min(void) {
    int32_t ret = INT32_MAX;
    for (uint32_t idx=0U; idx<this->length; idx++) {
        ret = MIN(ret, this->bin[idx]);
    }
    return ret;
}


void BinFilt::wr_idx_inc(void)
{
    if (++this->wr_idx >= this->size) {
        this->wr_idx = 0U;
    }
    // Overwrite logic
    if (this->wr_idx == this->rd_idx) {
         this->rd_idx_inc();
    }
}

void BinFilt::rd_idx_inc(void)
{
    if (++this->rd_idx >= this->size) {
        this->rd_idx = 0U;
    }
}
