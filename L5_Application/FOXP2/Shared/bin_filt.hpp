/*
 * Bin Filter
 * @author jtran
 * @version 0.1.0
 */

#ifndef BIN_FILT_H
#define BIN_FILT_H

#include <stdint.h>


class BinFilt
{
public:
    BinFilt(uint16_t bin_size);
    void insert(int32_t value);
    int32_t get_max(void);
    int32_t get_min(void);

private:
    void wr_idx_inc(void);
    void rd_idx_inc(void);
    uint16_t size;
    uint16_t length;
    uint16_t wr_idx;
    uint16_t rd_idx;
    int32_t *bin;
};


#endif  // BIN_FILT_H
