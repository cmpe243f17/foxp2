#ifndef DISCARD_FILT_H
#define DISCARD_FILT_H

#include <stdbool.h>
#include <stdint.h>


class DiscardFilt
{
public:
    DiscardFilt(uint32_t discard_threshold);
    bool check_gte(int32_t value, int32_t threshold);
private:
    bool check_counter_exceeded(void);
    uint32_t discard_thresh;
    uint32_t discard_counter;
};

#endif  // DISCARD_FILT_H
