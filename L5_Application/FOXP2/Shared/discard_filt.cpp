#include "discard_filt.hpp"

#include <stdio.h>
DiscardFilt::DiscardFilt(uint32_t discard_threshold)
{
    this->discard_thresh = discard_threshold;
    this->discard_counter = 0U;
}


bool DiscardFilt::check_gte(int32_t value, int32_t threshold)
{
    bool valid = false;
    if ((value >= threshold) || this->check_counter_exceeded()) {
        valid = true;
        this->discard_counter = 0U;
    }
    else {
        valid = false;
        this->discard_counter += 1;
    }
    return valid;
}


bool DiscardFilt::check_counter_exceeded(void)
{
    bool exceeded = false;
    if (this->discard_counter < this->discard_thresh) {
        exceeded = false;
    }
    else {
        exceeded = true;
    }
    return exceeded;
}
