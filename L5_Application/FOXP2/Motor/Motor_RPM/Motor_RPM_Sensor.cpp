/*
 *  Motor_RPM_Sensor.cpp
 *
 *  Created on: Oct 28, 2017
 *      Author: Ahsan Uddin
 *
 *  Driver for the RPM sensor needed to decode speed
 *
 */

// General includes
#include <stdio.h>


// BSP includes
#include "io.hpp"
#include "gpio.hpp"
#include "task.h"
#include "eint.h"

// FoxP2 specific includes
#include "Motor_LowLevel_Driver_Wrappers.hpp"
#if (!(UNIT_TEST == 1))
#include "generated_can.h"
#endif
#include "Motor_inc_private.hpp"
#include "Motor_Config.hpp"


#ifdef ECU_MOTOR

// With PWM frequency 420Hz and 65.75% as forward, 5 clicks is the optimum speed
#define NORMAL_FORWARD_TICKS            (5)     
#define WAIT_FOR_SLOW_SPEED_COUNT       (5)

#define FAST_SPEED              (NORMAL_FORWARD_TICKS * 1.5)
#define SLOW_SPEED              (NORMAL_FORWARD_TICKS / 2)

#define P_2_6        (6)         // Interrupt PIN for RPM sensor (P2.2)

#if (!(UNIT_TEST == 1))
static GPIO RPM_sensor_pin(P2_6);       // P2.6 PIN on board
#endif

static volatile uint16_t wheel_ticks = 0;

uint16_t get_wheel_ticks(void)
{
    return wheel_ticks;
}

void reset_wheel_ticks(void)
{
    MOTOR_CRITICAL_SECTION_ENTER();
    wheel_ticks = 0;
    MOTOR_CRITICAL_SECTION_EXIT();

    return;
}

void increment_wheel_ticks(void)
{
    MOTOR_CRITICAL_SECTION_ENTER();
    wheel_ticks += 1;
    MOTOR_CRITICAL_SECTION_EXIT();

    return;
}

void speed_check(float current_speed_ticks)
{

    motor_momentum_t current_momentum_state = get_motor_momentum();
    static uint8_t forward_counter = 1;     // Time marker for forward counter

    // We will only do speed check if we are really going forward!

    if (!(
        (current_momentum_state == FORWARD_FIRST_GEAR)  || 
        (current_momentum_state == FORWARD_SECOND_GEAR) || 
        (current_momentum_state == FORWARD_THIRD_GEAR)
        ))
    {
        forward_counter = 1;
        return;
    }
    else 
    {
        //Oh well, we are going forward actually so we will take care of business
        if (current_momentum_state != FORWARD_SECOND_GEAR)
        {
            forward_counter = 1;
        }
    }

    // Are we going too fast? 
    if (current_speed_ticks > FAST_SPEED)
    {
        // Let's slow down baby
        switch(current_momentum_state)
        {
            case FORWARD_THIRD_GEAR:
                // Bring it down to 2nd gear
                motor_drive_forward();
                break;

            case FORWARD_SECOND_GEAR:
                // Bring it down to 1st gear
                motor_drive_forward_slower();
                forward_counter = 1;
                break;

            default:
                break;
        }
    }

    // Or maybe we are going too slow actually?
    else if (current_speed_ticks < SLOW_SPEED)
    {
        // Let's gear up grandma
        switch(current_momentum_state)
        {
            case FORWARD_FIRST_GEAR:
                // Push it up to 2nd gear
                motor_drive_forward();
                break;

            case FORWARD_SECOND_GEAR:

                // The idea is that if we are in 2nd gear in slow speed, we have to hit back 
                // to back 5 times to gas it up. We have to do this because what if Gateway 
                // just asked us to go forward from stop and then we check the speed to find
                // out we are slow so we hit the gas? 
                if (forward_counter > WAIT_FOR_SLOW_SPEED_COUNT)
                {
                    // Push it up to 3rd gear
                    motor_drive_forward_faster();
                    forward_counter = 1;    
                }
                else
                {
                    forward_counter++;
                }
                break;

            default:
                break;
        }
    }

    else 
    {
        // If we reach here then it means we are really running at normal speed
        // So let's reset the forwar counter too then...
        forward_counter = 1;
    }

    return;
}

// It is assumed that this API will be called from 1 Hz handler
float get_wheel_rpm_in_1Hz(void)
{
    uint16_t current_ticks = get_wheel_ticks();
    float rpm = ( current_ticks / 1.0);
    reset_wheel_ticks();          // Reset the wheel ticks for next time
    return rpm;
}

void rpm_sensor_init(void)
{

#if (!(UNIT_TEST == 1))    
    RPM_sensor_pin.setAsInput();            /* Use this pin as INPUT */
    RPM_sensor_pin.enablePullDown();        /* Pull down enabled so that OFF state is really 0V */
#endif
    reset_wheel_ticks();
#if (!(UNIT_TEST == 1))
    eint3_enable_port2(P_2_6, eint_rising_edge, increment_wheel_ticks);
#endif
    return;
}

#endif  // MOTOR

