/*
 * Motor_CAN.cpp
 *
 *  Created on: Oct 21, 2017
 *      Author: Ahsan Uddin
 *
 *      Instructions/Notes
 *      1.  Enable Motor Board to emulate the Motor
 *      2.  In pre_build.sh, enable the dbc_parser for MOTOR and disable others
 *      3.  Every 100Hz (10ms), RX Q will be dequeued
 *      4.  If a valid value is received, it will be set the motor or the servo accordingly
 *      5.  Or else MIA handling will kick in
 *
 */

#include "stdio.h"
#include "gpio.hpp"
#include "io.hpp"

#include "generated_can.h"
#include "can.h"
#include "Build_Config.hpp"

#include "Motor_inc_private.hpp"
#include "Motor_Config.hpp"

#ifdef  ECU_MOTOR

const uint32_t               MOTOR_CMD__MIA_MS  = 1000;              // After 100 missed ones, kick in MIA
const MOTOR_CMD_t            MOTOR_CMD__MIA_MSG = {MOMENTUM_STOP, TURN_STRAIGHT};    // Data that will be set by default

const uint32_t               COMPASS_VALUE_CMD__MIA_MS = 1000;
const COMPASS_VALUE_CMD_t    COMPASS_VALUE_CMD__MIA_MSG = {0};

const uint32_t               GPS_VALUE_CMD__MIA_MS = 1000;
const GPS_VALUE_CMD_t        GPS_VALUE_CMD__MIA_MSG = {0};

static MOTOR_CMD_t  motor = {MOMENTUM_STOP, TURN_STRAIGHT};  		// Local and static variable
static COMPASS_VALUE_CMD_t  compass = {0};                          // Local but global variable
static GPS_VALUE_CMD_t      gps     = {0};                          // Local but global variable

void CAN_setup_high_level_filter(void)
{

    const can_std_id_t *slist       = NULL;
    // Send ACK for 50 - 150 only (Remember that we are interested only in MOTOR_CMDs which has ID 100)
    const can_std_grp_id_t sglist[] = {{CAN_gen_sid(can1, 50), CAN_gen_sid(can1, 150)}};
    const can_ext_id_t *elist       = NULL;            // We don't use any extended IDs
    const can_ext_grp_id_t *eglist  = NULL;            // We don't use any extended IDs

    CAN_setup_filter(slist, 0, sglist, 1, elist, 0, eglist, 0);

    return;
}

void motor_CAN_init(void)
{
    CAN_init(can1, CAN_BAUDRATE, RECEIVER_QUEUE_SIZE, SENDER_QUEUE_SIZE, NULL, NULL);
    CAN_setup_high_level_filter();
    CAN_reset_bus(can1);
    LD.clear();
    return;
}

uint8_t get_current_motor_momentum(void)
{
    return((uint8_t)(motor.MOTOR_CMD_MOMENTUM));
}

uint8_t get_current_motor_turn(void)
{
    return((uint8_t)(motor.MOTOR_CMD_TURN));
}

//send_bearing_to_LCD((uint32_t)(compass.COMPASS_VALUE_CMD_VALUE), (bool)(compass.COMPASS_VALUE_CMD_VALID));

bool get_compass_validity(void)
{
    return((bool)(compass.COMPASS_VALUE_CMD_VALID));
}

uint32_t get_compass_value(void)
{
    return((uint32_t)(compass.COMPASS_VALUE_CMD_VALUE));
}

bool get_gps_validity(void)
{
    return((bool)(gps.GPS_VALUE_CMD_VALID));
}

uint32_t get_gps_bearing(void)
{
    return((uint32_t)(gps.GPS_VALUE_CMD_BEARING));
}

uint32_t get_gps_distance(void)
{
    return((uint32_t)(gps.GPS_VALUE_CMD_DISTANCE));
}

void handle_CAN_receiver(void)
{
    can_msg_t can_received_msg;
    uint8_t LED_output = 0x00;    

    // If this LED is toggling, it means we are actively handling CAN reception messages
    LE.toggle(PERIODIC_LED);


    // Empty all of the queued, and received messages within the last 10ms (100Hz callback frequency)
    while (CAN_rx(can1, &can_received_msg, CAN_WAIT_VALUE))
    {
        // Form the message header from the metadata of the arriving message
        dbc_msg_hdr_t can_msg_hdr;
        can_msg_hdr.dlc = can_received_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_received_msg.msg_id;

        if (can_received_msg.msg_id == 100)     // Motor CMD is at 100 msg ID
        {

            // Attempt to decode the message (brute force, but should use switch/case with MID)
            dbc_decode_MOTOR_CMD(&motor, can_received_msg.data.bytes, &can_msg_hdr);
            handle_motor_momentum_and_turn();

        }

        else if (can_received_msg.msg_id == 140)        // Comapss CMD is at 140 message ID
        {
            dbc_decode_COMPASS_VALUE_CMD(&compass, can_received_msg.data.bytes, &can_msg_hdr);
        }

        else if (can_received_msg.msg_id == 145)        // Comapss GPS CMD is at 145 message ID
        {
            //bool dbc_decode_GPS_VALUE_CMD(GPS_VALUE_CMD_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
            dbc_decode_GPS_VALUE_CMD(&gps, can_received_msg.data.bytes, &can_msg_hdr);
        }

        else 
        {
            ASSERT_DEBUG(0);    // We got something other than Motor or Compass
        } 
      
        LED_output = motor.MOTOR_CMD_MOMENTUM + (motor.MOTOR_CMD_TURN * 10);
        LD.setNumber(LED_output);

    }   //while (CAN_rx(can1, &can_received_msg, CAN_WAIT_VALUE))
    
    // Here we will take care of MIA
    if (dbc_handle_mia_MOTOR_CMD(&motor, 10))
    {
        LD.setNumber(88); // MIA signature
    }
    
    if (dbc_handle_mia_COMPASS_VALUE_CMD(&compass, 10)) 
    {
        // Do something??
    }

    if (dbc_handle_mia_GPS_VALUE_CMD(&gps, 10))
    {
        // Do someting?
    }  

    return;
}

void handle_CAN_sender(uint32_t rpm_clicks)
{
    RPM_VALUE_CMD_t wheel_clicks = {0};

    wheel_clicks.RPM_VALUE_CMD_WHEEL_CLICKS = rpm_clicks;

    can_msg_t can_msg = { 0 };

    // Encode the CAN message's data bytes, get its header and set the CAN message's DLC and length    
    dbc_msg_hdr_t msg_hdr = dbc_encode_RPM_VALUE_CMD(can_msg.data.bytes, &wheel_clicks);    
    can_msg.msg_id = msg_hdr.mid;    
    can_msg.frame_fields.data_len = msg_hdr.dlc;

    if (CAN_tx(can1, &can_msg, CAN_WAIT_VALUE))    
    {        
        //Toggle LED if the msg submission succeeded        
        //LE.toggle(CAN_SEND_LED);    
    }    
    else    
    {        
        //LE.set(CAN_SEND_LED, 0);    
    }
    
    return;
}

void CAN_high_level_bus_reset(void)
{
    if(CAN_is_bus_off(can1))
    {
        CAN_reset_bus(can1);
        LE.toggle(CAN_BUS_RESET_LED);
    }
    // TODO: In final version, disable below! - Ahsan 11/5
    else
    {
        LE.set(CAN_BUS_RESET_LED,0);
    }
    
    return;
}

#endif  //MOTOR
