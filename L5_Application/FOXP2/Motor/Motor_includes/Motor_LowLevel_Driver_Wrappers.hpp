/*
 * Motor_LowLevel_Driver_Wrappers.hpp
 *
 *  Created on: Nov 19, 2017
 *      Author: Ahsan Uddin
 *
 *	I had no intention of creating these wrappers but for Unit testing purpose
 * 	I couldn't find any other way to hide the drivers so here we are!
 *
 */

#ifndef MOTOR_LL_DRIVER_WRAPPER_
#define MOTOR_LL_DRIVER_WRAPPER_

#if (!(UNIT_TEST == 1))

#define MOTOR_SET_PWM(pwm)	back_motor.set(input_PWM_percentage)
#define SERVO_SET_PWM(pwm)	front_servo.set(input_PWM_percentage);

#define	MOTOR_CRITICAL_SECTION_ENTER()						taskENTER_CRITICAL()
#define	MOTOR_CRITICAL_SECTION_EXIT()						taskEXIT_CRITICAL()

#define LED_CLEAR()											LD.clear()
#define LED_SET(value)										LD.setNumber(value)
#define UART_INIT(baud_rate,tx_buff_size, rx_buff_size)		Uart2::getInstance().init(baud_rate, tx_buff_size, rx_buff_size)
#define UART_PUT(value,timeout)								Uart2::getInstance().putChar(value,timeout)
#define UART_GET(rx_buff, timeout)							Uart2::getInstance().getChar(rx_buff, timeout);
	
#else

#define MOTOR_SET_PWM(pwm)
#define SERVO_SET_PWM(pwm)

#define	MOTOR_CRITICAL_SECTION_ENTER()		
#define	MOTOR_CRITICAL_SECTION_EXIT()

#define LED_CLEAR()
#define LED_SET(value)
#define UART_INIT(baud_rate,tx_buff_size, rx_buff_size)	
#define UART_PUT(value,timeout)		
#define UART_GET(rx_buff, timeout)	

#endif	// UNIT_TEST == 1

#endif