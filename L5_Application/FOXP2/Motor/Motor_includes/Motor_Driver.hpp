/*
 * Motor_Driver.hpp
 *
 *  Created on: Oct 11, 2017
 *      Author: Ahsan
 */

#ifndef MOTOR_DRIVER_HPP_
#define MOTOR_DRIVER_HPP_

void motor_init(void);
void periodic_motor_1HZ(void);
void periodic_motor_10HZ(void);
void periodic_motor_100HZ(void);
void periodic_motor_1KHZ(void);

void motor_low_priority_init(void);
void low_priority_motor_1Hz(unsigned int count);

#endif /* MOTOR_DRIVER_HPP_ */
