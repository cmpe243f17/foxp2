/*
 * Motor_Config.hpp
 *
 *  Created on: Oct 20, 2017
 *      Author: Ahsan Uddin
 */

#ifndef MOTOR_CONFIG_HPP_
#define MOTOR_CONFIG_HPP_

/*

    <<< OEM spec >>>
    RC receiver's spec:
    1. Frequency:       100Hz   (9.91ms)
    2. Idle:            1.50ms  (15.13%)
    3. Full forward:    1.98ms  (19.97%)
    4. Full backward:   1.17    (11.80%)

    <<< 107.8Hz observed spec >>>
    Motor Implementation using 107.8Hz
    1. Frequency settings for PWM: 9
    2. Idle/Init:       1.48ms  (16.00%)
    3. Forward:         1.575ms (17.97%)
    4. Brake:           834us   (9.80)

    Servo Implementation using 107.9HzHz
    1. Frequency settings for PWM: 9
    2. Straight:        1.48ms  (16.00%)
    3. Right:           1.95ms  (21.50%)
    4. Left:            926us   (10.50%)

    <<< 419.5Hz observed spec >>>
    Motor Implementation using 419.5Hz
    1. Frequency settings for PWM: 35
    2. Idle/Init:       1.50ms  (63%)
    3. Slow Forward:    1.57ms  (66%)
    4. Fast Forward:    1.59ms  (67%)
    5. Slow Reverse:    1.42ms  (60%)
    6. Fast reverse:    1.40ms  (59%)

    Servo Implementation using 419.5Hz
    1. Frequency settings for PWM: 35
    2. Straight:        1.48ms  (62%)
    3. Right:           1.95ms  (82%)
    4. Left:            929us   (39%)

*/


// Originally PWM 35 was giving us 420Hz. After bugfix, we can directly use 420 Hz
#define PWM_FREQUENCY       (420)

#if  (PWM_FREQUENCY == 420)
#define SERVO_STRAIGHT_PWM  (62.00)
#define SERVO_RIGHT_PWM     (82.00)
#define SERVO_LEFT_PWM      (39.00)

#define MOTOR_FORWARD_PWM   (65.75)     // 67% is fast forward
#define MOTOR_REVERSE_PWM   (59.00)
#define MOTOR_IDLE_PWM      (63.00)
#define MOTOR_BRAKE_PWM     (63.00)     // Need to experiment more!!!

#else
#error  Unknown frequency for PWM!!!
#endif

// Output LED defines
#define PERIODIC_LED                (1)     
//#define CAN_SEND_LED              (2) 
#define MOTOR_SLOWER_FEEDBACK_ON  (2)
#define MOTOR_FASTER_FEEDBACK_ON  (3)
#define CAN_BUS_RESET_LED           (4)

// LCD Specifics
enum
{
    LCD_BEARING_TEXT    = 0x0,
    LCD_HEADING_TEXT    = 0x3,
    LCD_DISTANCE_TEXT   = 0x4,
};

enum
{
    GPS_OBJ         = 0x0,
    FW_OBJ          = 0x1,
    GIT_HASH_OBJ    = 0x2,
    SPEED_OBJ       = 0x7,
    VOLTAGE_OBJ     = 0xB,
};

#define CONVERT_TO_STRING2(X)   #X
#define CONVERT_TO_STRING(X)    CONVERT_TO_STRING2(X)

typedef enum
{
    NO_REVERSE          = 0x0,
    REVERSE_REQUESTED   = 0x1,
    FIRST_IDLE          = 0x2,
    FIRST_REVERSE       = 0x3,
    SECOND_IDLE         = 0x4,
    SECOND_REVERSE      = 0x5,
    REVERSE_DONE        = 0x6
}Reverse_Current_State_t;

typedef enum{
    STOP                = 0x0,
    FORWARD_FIRST_GEAR  = 0x1,
    FORWARD_SECOND_GEAR = 0x2,
    FORWARD_THIRD_GEAR  = 0x3,
    BACKWARD            = 0x4,
    INVALID_MOMENTUM    = 0x5
}motor_momentum_t;

#endif /* MOTOR_CONFIG_HPP_ */
