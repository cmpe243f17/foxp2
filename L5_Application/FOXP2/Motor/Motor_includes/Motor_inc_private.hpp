/*
 * Motor_inc_private.hpp
 *
 *  Created on: Oct 11, 2017
 *      Author: Ahsan Uddin
 */

#ifndef MOTOR_INC_PRIVATE_HPP_
#define MOTOR_INC_PRIVATE_HPP_

#include "Motor_Config.hpp"

void motor_CAN_init(void);
void CAN_high_level_bus_reset(void);
void handle_CAN_receiver(void);
void handle_CAN_sender(uint32_t rpm_clicks);

void motor_PWM_init(void);
void handle_motor_momentum_and_turn(void);

uint8_t get_current_motor_momentum(void);
uint8_t get_current_motor_turn(void);

bool get_compass_validity(void);
uint32_t get_compass_value(void);

bool get_gps_validity(void);
uint32_t get_gps_bearing(void);
uint32_t get_gps_distance(void);

void motor_drive_forward(void);
void motor_drive_forward_faster(void);
void motor_drive_forward_slower(void);
void request_reverse(void);
bool is_reverse_requested(void);
bool is_reverse_ongoing(void);
void reset_reverse_state(void);
void set_reverse_state(Reverse_Current_State_t new_state);
Reverse_Current_State_t get_reverse_state(void);
void motor_drive_reverse(void);
void motor_drive_IDLE_or_STOP(void);

void turn_servo_left(void);
void turn_servo_right(void);
void turn_servo_straight(void);

//LCD codes
void LCD_init(void);
bool send_data(char object_ID, char object_index, int data);
bool send_bearing_to_LCD(char object_index, uint32_t value, bool gps_locked);
bool send_static_text(char object_index, char * string);

// RPM sensor
void rpm_sensor_init(void);          
float get_wheel_rpm_in_1Hz(void);

// Voltage sensor 
void voltage_sensor_init(void);
float get_voltage(void);

// Speed feedback
motor_momentum_t get_motor_momentum(void);
void speed_check(float current_speed_ticks);

// Debugging includes. Needs to be cleaned up later on....
void handle_manual_PWM(void);
void PWM_fine_tuning(void);

#endif /* MOTOR_INC_PRIVATE_HPP_ */
