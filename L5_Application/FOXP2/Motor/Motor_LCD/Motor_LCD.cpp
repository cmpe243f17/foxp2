/*
 * Motor_LCD.cpp
 *
 *  Created on: Oct 30, 2017
 *      Author: Sophia Quan
 */


#include "stdio.h"
#include "stdlib.h"
#include "uart2.hpp"
#include "utilities.h"
#include <string.h>
#include "gpio.hpp"
#include "io.hpp"

#include "Build_Config.hpp"
#include "Motor_LowLevel_Driver_Wrappers.hpp"

#ifdef ECU_MOTOR

#define LCD_UART_BAUD	(9600)
#define LCD_TX_SIZE		(1000)
#define LCD_RX_SIZE		(1000)

void LCD_init(void)
{
	UART_INIT(LCD_UART_BAUD, LCD_TX_SIZE, LCD_RX_SIZE);
	return;
}

void received_data(char *rxBuffer)
{
	UART_GET(rxBuffer, 100);
}

bool if_ACK(void)
{
	char buf;
	received_data(&buf);
	if(buf == 6)
	{
		return true;
	}
	else
	{
		return false;
	}

}

bool if_NACK(void)
{
	char buf;
	received_data(&buf);
	if(buf == 21)
	{
		return true;
	}
	else
	{
		return false;
	}

}

bool send_data(char object_ID, char object_index, int data)
{
	int checksum;
	int write_cmd = 1;
	char msb, lsb;
	lsb = data & 0xff;
	msb = (data >> 8) & 0xff;

	if ((data < 0) || (data > 50))
	{
		return false;
	}

	UART_PUT(write_cmd, 0);
	UART_PUT(object_ID, 0);
	UART_PUT(object_index, 0);
	UART_PUT(msb, 0);
	UART_PUT(lsb, 0);
	checksum = write_cmd ^ object_ID ^ object_index ^ 0 ^ data;
	UART_PUT(checksum, 0);
	
#if 0
	if (if_ACK())
	{
		printf("Successfully transmitted data!\n");
	}
	else if (if_NACK())
	{
		printf("NACK received.\n");
	}
	else
	{
		printf("Invalid data sent to LCD.\n");
	}
#endif	

	return true;
}

char return_char[5] = {0};

char * numbers_to_ASCII(uint32_t number, int * counter)
{
	int number_1, number_2, number_3;

	memset(&return_char[0], 0x00, 5*(sizeof(char)));

	number_1 = number / 100;
	number_2 = (number - (100 * number_1)) / 10;
	number_3 = (number - (100 * number_1) - (10 * number_2)) / 1;

	return_char[(*counter)++] = number_1 + '0';
	return_char[(*counter)++] = number_2 + '0';
	return_char[(*counter)++] = number_3 + '0';

	return &return_char[0];
}

bool send_bearing_to_LCD(char object_index, uint32_t value, bool gps_locked)
{
	//char object_index = LCD_BEARING_TEXT;
	int checksum = 0;
	int counter = 0;
	int write_cmd = 2;

	if((object_index == 0x0) || (object_index == 0x3))
	{
		if(value > 360)
		{
			return false;
		}
	}
	
	char *char_string = numbers_to_ASCII(value, &counter);

	if (gps_locked)
	{
		UART_PUT(write_cmd, 0);
		UART_PUT(object_index, 0);
		UART_PUT(counter+1, 0);

		for (int i = 0; i < counter; i++)
		{
			UART_PUT(char_string[i], 0);
			checksum ^= char_string[i];
		}

		UART_PUT(0x0, 0);
		checksum ^= (write_cmd ^ object_index ^ (counter + 1) ^ 0x00);
		UART_PUT(checksum, 0);
	}
	else
	{
		// Hardcoding the text in the LCD to "Invalid"
		UART_PUT(write_cmd, 0);
		UART_PUT(object_index, 0);
		UART_PUT(0x09, 0);
		UART_PUT(0x69, 0);
		UART_PUT(0x6E, 0);
		UART_PUT(0x76, 0);
		UART_PUT(0x61, 0);
		UART_PUT(0x6C, 0);
		UART_PUT(0x69, 0);
		UART_PUT(0x64, 0);
		UART_PUT(0x21, 0);
		UART_PUT(0x00, 0);

		if (object_index == 0x0)
		{
			UART_PUT(0x5B, 0);
		}
		else if (object_index == 0x3)
		{
			UART_PUT(0x58, 0);
		}
		else if (object_index == 0x4)
		{
			UART_PUT(0x5f, 0);
		}
		else
		{
			// Some unknown IDs maybe?
			ASSERT_DEBUG(0);
		}
	}

#if 0
	if (if_ACK())
	{
		printf("Successfully transmitted data!\n");
	}
	else if (if_NACK())
	{
		printf("NACK received.\n");
	}
	else
	{
		printf("Invalid data sent to LCD.\n");
	}
#endif
	return true;
}

bool send_static_text(char object_index, char * string)
{
	int write_cmd = 2;
	int checksum = 0;

	UART_PUT(write_cmd, 0);
	UART_PUT(object_index, 0);
	UART_PUT((strlen(string)+1), 0);

    for (uint8_t i = 0; i < strlen(string); i++)
    {
        UART_PUT((int)string[i], 0);
        checksum ^= (int)string[i];
    }

    UART_PUT(0x0, 0);
	checksum ^= (write_cmd ^ object_index ^ (strlen(string)+1) ^ 0x00);
	UART_PUT(checksum, 0);

	return true;
}

#endif
