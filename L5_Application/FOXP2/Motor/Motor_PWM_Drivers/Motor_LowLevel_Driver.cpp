/*
 *  Motor_LowLevel_Driver.cpp
 *
 *  Created on: Oct 11, 2017
 *      Author: Ahsan Uddin
 *
 *  All the low level Motor controls including the classes and structs
 *  are in this file. Appropriate wrappers are created which can be used
 *  in the high level motor driver
 *
 */

#include "stdlib.h"
#include "stdio.h"
#include <stdint.h>

#if (!(UNIT_TEST == 1))
#include "gpio.hpp"
#include "io.hpp"
#include "lpc_pwm.hpp"

#include "generated_can.h"
#endif  // UNIT_TEST == 1

#include "Build_Config.hpp"
#include "singleton_template.hpp"

#include "Motor_LowLevel_Driver_Wrappers.hpp"
#include "Motor_inc_private.hpp"
#include "Motor_Config.hpp"

#ifdef ECU_MOTOR

motor_momentum_t motor_momentum = STOP;

motor_momentum_t get_motor_momentum(void)
{
    return motor_momentum;
}

void set_motor_momentum(motor_momentum_t new_momentum)
{
    motor_momentum = new_momentum;
    return;
}


class Motor_Control: public SingletonTemplate<Motor_Control>
{
    public:
        // Initializing PWM1 @ P2.0 for motor(esc)
        // Initializing PWM2 @ P2.1 for servo(direction)
        // PWM3 (P2.6) is reserved for RPM sensor
        void pwm_init(void)
        {
            // This is a dummy callback that will make sure we constructed our PWM class
            return;
        }

        // Low level APIs for Front wheels turning
        void turnLeft(void)
        {
            this->setTurnServo(SERVO_LEFT_PWM);
            return;
        }

        void turnRight(void)
        {
            this->setTurnServo(SERVO_RIGHT_PWM);
            return;
        }

        void turnStraight(void)
        {
            this->setTurnServo(SERVO_STRAIGHT_PWM);
            return;
        }

        // Low level APIs for Back Motor Control
        void driveForward(void)
        {
            this->setDriveMotor(MOTOR_FORWARD_PWM);
            return;
        }

        void driveForwardFaster(void)
        {
            this->setDriveMotor(MOTOR_FORWARD_PWM + 0.50);
            return;
        }

        void driveForwardSlower(void)
        {
            this->setDriveMotor(MOTOR_FORWARD_PWM - 0.50);
            return;
        }

        void driveReverse(void)
        {
            this->setDriveMotor(MOTOR_REVERSE_PWM);
            return;
        }

        void driveIDLEorSTOP(void)
        {
            this->setDriveMotor(MOTOR_IDLE_PWM);
            return;
        }

    private:

#if (!(UNIT_TEST == 1))
        PWM back_motor, front_servo;
        Motor_Control() : back_motor(PWM::pwm1, PWM_FREQUENCY), front_servo(PWM::pwm2, PWM_FREQUENCY)
#else
        Motor_Control()
#endif
        
        {
            // Set the ESC to IDLE for initialization sequence
            this->driveIDLEorSTOP();
            // Turn the front wheels straight
            this->turnStraight();
        }

        void setDriveMotor(float input_PWM_percentage)
        {
            if  (input_PWM_percentage <= 100)
            {
                //back_motor.set(input_PWM_percentage);
                MOTOR_SET_PWM(input_PWM_percentage);
            }
            return;
        }

        void setTurnServo(float input_PWM_percentage)
        {
            if  (input_PWM_percentage <= 100)
            {
                //front_servo.set(input_PWM_percentage);
                SERVO_SET_PWM(input_PWM_percentage);
            }
            return;
        }

        friend class SingletonTemplate<Motor_Control>;
};



// API for initializing the PWMs for Motor and Servo
void motor_PWM_init(void)
{
    Motor_Control::getInstance().pwm_init();
    LED_CLEAR();
return;
}

//  High Level Motor Control APIs
void motor_drive_forward(void)
{
    Motor_Control::getInstance().driveForward();
    set_motor_momentum(FORWARD_SECOND_GEAR);
    return;
}

void motor_drive_forward_faster(void)
{
    Motor_Control::getInstance().driveForwardFaster();
    set_motor_momentum(FORWARD_THIRD_GEAR);
    return;
}

void motor_drive_forward_slower(void)
{
    Motor_Control::getInstance().driveForwardSlower();
    set_motor_momentum(FORWARD_FIRST_GEAR);
    return;
}



// TODO: Cleanup the printf and the LDs - Ahsan 10/22

void motor_drive_reverse(void)
{

    Reverse_Current_State_t current_reverse_state = get_reverse_state();
    // This API should be called only when we know that we need reversal
    ASSERT_DEBUG(current_reverse_state != NO_REVERSE);

    //printf("Reverse State: %d\n", counter, current_reverse_state);

    set_motor_momentum(BACKWARD);

    switch(current_reverse_state)
    {

        case REVERSE_REQUESTED:
            Motor_Control::getInstance().driveIDLEorSTOP();
            LED_SET(REVERSE_REQUESTED);
            set_reverse_state(FIRST_IDLE);
            break;

        case FIRST_IDLE:
            Motor_Control::getInstance().driveReverse();
            LED_SET(FIRST_IDLE);
            set_reverse_state(FIRST_REVERSE);
            break;

        case FIRST_REVERSE:
            Motor_Control::getInstance().driveIDLEorSTOP();
            LED_SET(FIRST_REVERSE);
            set_reverse_state(SECOND_IDLE);
            break;

        case SECOND_IDLE:
            Motor_Control::getInstance().driveReverse();
            LED_SET(SECOND_IDLE);
            set_reverse_state(SECOND_REVERSE);
            break;

        case SECOND_REVERSE:
            // Second reverse is done, by now, the motor should be reversing
            LED_SET(SECOND_REVERSE);
            set_reverse_state(REVERSE_DONE);
            break;

        case REVERSE_DONE:
            // resetting the reverse_state state machine
            LED_SET(REVERSE_DONE);
            reset_reverse_state();
            break;

        default:
            // We didn't miss anymore states right??
            LED_SET(56);
            ASSERT_DEBUG(0);
            break;

    }

    return;
}

void motor_drive_IDLE_or_STOP(void)
{
    Motor_Control::getInstance().driveIDLEorSTOP();
    set_motor_momentum(STOP);
    return;
}

//  High level Servo turns API
void turn_servo_left(void)
{
    Motor_Control::getInstance().turnLeft();
    return;
}

void turn_servo_right(void)
{
    Motor_Control::getInstance().turnRight();
    return;
}

void turn_servo_straight(void)
{
    Motor_Control::getInstance().turnStraight();
    return;
}

#endif  // MOTOR
