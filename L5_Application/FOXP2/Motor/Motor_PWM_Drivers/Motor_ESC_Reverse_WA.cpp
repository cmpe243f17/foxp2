/*
 *  Motor_ESC_Reverse_WA.cpp
 *
 *  Created on: Oct 22, 2017
 *      Author: Ahsan Uddin
 *
 *  In the Traxxas ESC that we are using, we can not directly jump into reversing.
 *  Instead, we have to do the following sequence:
 *      1. IDLE
 *      2. REVERSE
 *      3. IDLE
 *      4. REVERSE
 *  In this file we wll save the states of each of the above steps.
 *
 */

#ifndef MOTOR_UNIT_TEST
#include "generated_can.h"
#include "io.hpp"
#include "gpio.hpp"
#include "task.h"
#endif
#include "Motor_LowLevel_Driver_Wrappers.hpp"
#include "Build_Config.hpp"
#include "Motor_Config.hpp"

#ifdef ECU_MOTOR               //This code is for the Reverse logic WA

static volatile Reverse_Current_State_t reverse_state = NO_REVERSE;

void set_reverse_state(Reverse_Current_State_t new_state)
{
    MOTOR_CRITICAL_SECTION_ENTER();
    reverse_state = new_state;
    MOTOR_CRITICAL_SECTION_EXIT();
    return;
}

Reverse_Current_State_t get_reverse_state(void)
{
    return reverse_state;
}

void reset_reverse_state(void)
{
    set_reverse_state(NO_REVERSE);
    return;
}

bool is_reverse_requested(void)
{
    return (get_reverse_state() == REVERSE_REQUESTED);
}

bool is_reverse_ongoing(void)
{
    return (get_reverse_state() != NO_REVERSE);
}

void request_reverse(void)
{
    // If we are reversing that means we are not in the middle of one already
    ASSERT_DEBUG(get_reverse_state() == NO_REVERSE);
    set_reverse_state(REVERSE_REQUESTED);
    return;
}

#endif

