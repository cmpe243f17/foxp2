/*
 * Motor_Debug.cpp
 *
 *  Created on: Oct 21, 2017
 *      Author: Ahsan Uddin
 */

#include <stdint.h>
#include "gpio.hpp"
#include "io.hpp"

#include "generated_can.h"
#include "Motor_inc_private.hpp"


#ifdef ECU_MOTOR

// Below APIs are for debugging only and needs to be moved to somewhere else

/*
 *      Use this API only when debugging from single board
 *      Turn the callback at periodic_motor_100HZ()
 *      Make sure handle_CAN_receiver() and PWM_fine_tuning() is switched off
 */

void handle_manual_PWM(void)
{

    if (SW.getSwitch(1))
    {
        motor_drive_IDLE_or_STOP();     // Idle the motor aka stop
        turn_servo_straight();        // Turn straight
        LD.setNumber(10);
    }
    else if (SW.getSwitch(2))
    {
        motor_drive_forward();          // Drive car forward
        turn_servo_right();           // Turn Right
        LD.setNumber(20);
    }
    else if (SW.getSwitch(3))
    {
        //motor_drive_reverse();          // Drive car reverse
        if (!is_reverse_ongoing())
        {
            request_reverse();
            LD.setNumber(30);
        }
        turn_servo_left();            // Turn Left

    }
    else if (SW.getSwitch(4))
    {
        LD.setNumber(40);
    }

    return;
}

/*
 *      Special API for fine tuning PWM
 *      Turn the callback at periodic_motor_1HZ()
 *      Make sure handle_CAN_receiver() and handle_manual_PWM() is switched off
 */
void PWM_fine_tuning(void)
{

    if (SW.getSwitch(1))
    {
        LD.setNumber(72);
    }
    else if (SW.getSwitch(2))
    {
        LD.setNumber(75);
    }
    else if (SW.getSwitch(3))
    {
        LD.setNumber(69);
    }
    else if (SW.getSwitch(4))
    {
        LD.setNumber(68);
    }

    return;
}

#endif
