/*
 * Motor_Periodic_Callbacks.cpp
 *
 *  Created on: Oct 9, 2017
 *      Author: Ahsan Uddin
 *
 *  This file acts as a periodic dispatcher for the Motor side
 *
 */

#include "stdio.h"
#include "stdlib.h"
#include "generated_can.h"
#include "Motor_inc_private.hpp"
#include "io.hpp"

#ifdef ECU_MOTOR

// Turn this on when debugging manually
#define noMOTOR_DEBUG
#define noSPEED_CHECK
#define LCD
        
void motor_init()
{

    char FW_rev[50];
    char FW_Branch[50];

#ifndef MOTOR_DEBUG
    motor_CAN_init();      // Turn on when doing CAN stuff
#endif
    motor_PWM_init();
    reset_reverse_state();
#ifdef LCD   
    LCD_init();
    
    sprintf(FW_rev, "FW Rev: %s", CONVERT_TO_STRING(FW_VERSION));
    sprintf(FW_Branch, "Git Branch: %s", CONVERT_TO_STRING(BRANCH_INFO));

    send_static_text(FW_OBJ, FW_rev);
    send_static_text(GIT_HASH_OBJ, FW_Branch);
#endif
    rpm_sensor_init();
    voltage_sensor_init();
    return;
}

void periodic_motor_1HZ()
{
    float rpm_clicks = 0;
#ifndef MOTOR_DEBUG
    CAN_high_level_bus_reset();       // Turn on when doing CAN stuff
#else
    //PWM_fine_tuning();
    handle_manual_PWM();
#endif
    //printf("Current Voltage: %0.3f V\n", get_voltage());
    rpm_clicks = get_wheel_rpm_in_1Hz();
    //printf("Current speed: %0.3f clicks/sec\n", rpm_clicks);

#ifdef SPEED_CHECK
    speed_check(rpm_clicks);
#endif   

#if 0
    printf("Compass validity: %s\n", (compass.COMPASS_VALUE_CMD_VALID == 1) ? "TRUE":"FALSE");
    printf("Compass value: %0.3f\n", compass.COMPASS_VALUE_CMD_VALUE);
    printf("\n");
#endif

#ifdef LCD
    send_data(SPEED_OBJ,0, (uint16_t)rpm_clicks);
    send_bearing_to_LCD(LCD_BEARING_TEXT, get_compass_value(), get_compass_validity());
    send_bearing_to_LCD(LCD_HEADING_TEXT, get_gps_bearing(), get_gps_validity());
    send_bearing_to_LCD(LCD_DISTANCE_TEXT, get_gps_distance(), get_gps_validity());
#endif    

#ifndef MOTOR_DEBUG
    handle_CAN_sender((uint16_t)rpm_clicks);
#endif

    return;
}

void periodic_motor_10HZ()
{
    /*
     * We found out that due to security of the motor, the motor can not jump back to reversal
     * So we put a WA to stop the motor internally and reverse it in steps
     * See Issue# 1 in the repo (https://gitlab.com/foxp2/foxp2/issues/1)
     */

    if (is_reverse_requested() || is_reverse_ongoing())
    {
        motor_drive_reverse();
    }
    
    return;
}

void periodic_motor_100HZ()
{
#ifndef MOTOR_DEBUG
    handle_CAN_receiver();
#endif
    return;
}

void periodic_motor_1KHZ()
{
    
}

void motor_low_priority_init(void)
{

}

void low_priority_motor_1Hz(unsigned int count)
{

}

#endif  // MOTOR
