/*
 * Motor_HighLevel_Driver.cpp
 *
 *  Created on: Nov 26, 2017
 *      Author: Ahsan Uddin
 *
 *  This file contains the high level driver for the ECU_MOTOR 
 *
 */

#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "Motor_inc_private.hpp"
#include "Build_Config.hpp"

#ifdef ECU_MOTOR

void handle_motor_momentum_and_turn(void)
{
    static uint8_t current_motor_state_machine = ABSOLUTE_MOMENTUM_MAX + (ABSOLUTE_TURN_MAX << 4); // Basically this should be 0xFF// Momentum handlings

    // There is a reverse ongoing, we can not handle anything right now.
    // Master needs to commit that they wouldn't send anything for the next 500 msec!           
    if (!is_reverse_ongoing())
    {
        switch(get_current_motor_momentum())
        {
            case MOMENTUM_NOP:
                // Do nothing
                break;

            case MOMENTUM_FORWARD:
                if (current_motor_state_machine != MOMENTUM_FORWARD)
                {
                    motor_drive_forward();
                    current_motor_state_machine = MOMENTUM_FORWARD;
                }
                break;

            case MOMENTUM_REVERSE:

                if (current_motor_state_machine != MOMENTUM_REVERSE)
                {
                    //motor_drive_reverse();
                    request_reverse();
                    current_motor_state_machine = MOMENTUM_REVERSE;
                }
                break;

            case MOMENTUM_STOP:

                motor_drive_IDLE_or_STOP();
                current_motor_state_machine = MOMENTUM_STOP;
                break;

            default:
                // We shouldn't come here. Are we missing any more CMD in Momentum?
                ASSERT_DEBUG(0);
                break;    

        }        
    }

    switch(get_current_motor_turn())
    {
        case TURN_NOP:
            // Do nothing
            break;

        case TURN_STRAIGHT:
            turn_servo_straight();
            break;

        case TURN_LEFT:
            turn_servo_left();
            break;

        case TURN_RIGHT:
            turn_servo_right();

        default:
            // What the hell did we miss?
            ASSERT_DEBUG(0);
            break;
    }    

    return;
}

#endif  // ECU_MOTOR