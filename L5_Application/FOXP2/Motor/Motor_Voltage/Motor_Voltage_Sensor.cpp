/*
 *  Motor_RPM_Sensor.cpp
 *
 *  Created on: Oct 28, 2017
 *      Author: Ahsan Uddin
 *
 *  Driver for the RPM sensor needed to decode speed
 *
 */

// General includes
#include <stdio.h>


// BSP includes
#include "io.hpp"
#include "gpio.hpp"
#include "task.h"
#include "adc0.h"

// FoxP2 specific includes
#include "generated_can.h"

#ifdef ECU_MOTOR

void voltage_sensor_init(void)
{
    
    LPC_PINCON->PINSEL1 |= (1 << 20);           // ADC3 @ P0.26
    return;
}

int low_level_adc_read(void)
{
    int raw_value = adc0_get_reading(3);

    return raw_value;
}

/*
 *  Our magical formula is y = 87x + 3118 !
 */
float get_voltage(void)
{
    int voltage = low_level_adc_read();
    float calulated_voltage = ((voltage - 3118) / 87.0); 

    // Note that if input voltage is 0V, our sensor will still send the offset
    if (calulated_voltage < 0)
    {
        calulated_voltage = 0;
    }

    return calulated_voltage;
}

#endif  // MOTOR

 
