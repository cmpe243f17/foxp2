#include "carprox.hpp"

#include "bin_filt.hpp"
#include "Build_Config.hpp"
#include "discard_filt.hpp"
#include "gpio.hpp"
#include "io.hpp"
#include "mb1010.hpp"


typedef struct {
    BinFilt *bin_filt;
    DiscardFilt *discard_filt;
    MB1010 *usr;
    GPIO *led_indicator;
} ProxHandle_S;

typedef struct {
    ProxHandle_S handles[CARPROX_COUNT];
} CarproxVars_S;

CarproxVars_S vars = {0};

static void updated_distances(void);
static void debug_display_distance(void);
static void debug_update_led(void);


void CARPROX_init(void)
{
    LPC_PINCON->PINSEL1 |= (1 << 20);  // P0.26 - ch3
    LPC_PINCON->PINSEL3 |= (3 << 28);  // P1.30 - ch4
    LPC_PINCON->PINSEL3 |= (3 << 30);  // P1.31 - ch5

    for (uint8_t idx=0U; idx<(uint8_t)CARPROX_COUNT; idx++) {
        switch (idx) {
            case CARPROX_FRONT:
                vars.handles[idx].usr = new MB1010(3, P1_19);  // ADC channel = 3
                vars.handles[idx].led_indicator = new GPIO(P2_3);
                break;
            case CARPROX_LFRONT:
                vars.handles[idx].usr = new MB1010(5, P1_19);  // ADC channel = 5
                vars.handles[idx].led_indicator = new GPIO(P2_2);
                break;
            case CARPROX_RFRONT:
                vars.handles[idx].usr = new MB1010(4, P1_19);  // ADC channel = 4
                vars.handles[idx].led_indicator = new GPIO(P2_4);
                break;
            default:
                break;
        }
        vars.handles[idx].bin_filt = new BinFilt(10U);
        vars.handles[idx].discard_filt = new DiscardFilt(1U);
        vars.handles[idx].led_indicator->setAsOutput();
    }
    vars.handles[0U].usr->init();  // Call init for 1 sensor only
}


void CARPROX_task_10hz(void)
{
    debug_display_distance();
}


void CARPROX_task_100hz(void)
{
    updated_distances();
    debug_update_led();
}


uint32_t CARPROX_get_distance_in(CARPROXIndex_E index)
{
    return (uint32_t)vars.handles[index].bin_filt->get_min();
}


static void updated_distances(void)
{
    for (uint8_t idx=0U; idx<(uint8_t)CARPROX_COUNT; idx++) {
        vars.handles[idx].usr->update_distance();
        uint32_t updated_distance = vars.handles[idx].usr->get_distance();
        if (vars.handles[idx].discard_filt->check_gte((int32_t)updated_distance, SENSOR_PROX_DISTANCE_THRESH_IN)) {
            vars.handles[idx].bin_filt->insert((int32_t)updated_distance);
        }
        //vars.handles[idx].bin_filt->insert((int32_t)updated_distance);
    }
}


static void debug_display_distance(void)
{
    static CARPROXIndex_E sensor_idx = CARPROX_FRONT;
    
    if (SW.getSwitch(1)) {
        sensor_idx = CARPROX_FRONT;
    }
    else if (SW.getSwitch(2)) {
        sensor_idx = CARPROX_LFRONT;
    }
    else if (SW.getSwitch(3)) {
        sensor_idx = CARPROX_RFRONT;
    }
    else {
        // Pass
    }

    uint32_t distance = (uint32_t)vars.handles[sensor_idx].bin_filt->get_min();
    LD.setNumber((distance < 100U ? distance : 99U));
}


static void debug_update_led(void)
{
    for (uint8_t idx=0U; idx<(uint8_t)CARPROX_COUNT; idx++) {
        if ((uint32_t)vars.handles[idx].bin_filt->get_min() < SENSOR_PROX_DISTANCE_THRESH_IN) {
            vars.handles[idx].led_indicator->setHigh();
        }
        else {
            vars.handles[idx].led_indicator->setLow();
        }
    }
}
