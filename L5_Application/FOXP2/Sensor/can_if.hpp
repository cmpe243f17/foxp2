/*
 * CAN Interface
 * @author jtran
 * @version 1.0.0
 */

#ifndef CAN_IF_HPP
#define CAN_IF_HPP

void CANIF_init(void);
void CANIF_task_1hz(void);
void CANIF_task_10hz(void);

#endif  // CAN_IF_HPP
