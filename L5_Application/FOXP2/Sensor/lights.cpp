/*
 * lights.cpp
 *
 *  Created on: Nov 25, 2017
 *      Author: Taylor
 */
#include <stdio.h>
#include <inttypes.h>

#include "io.hpp"
#include "lights.hpp"
#include "lpc_pwm.hpp"

#ifdef ECU_SENSOR

/// NOTE: THIS MODULE IS NOT THREAD SAFE, BUT IT'S JUST LEDS SO WHO CARES

/// Sets the brightness of the headlights / taillights based off of the detected brightness
static void lights_set_by_ambient_light(void);

/**
 * Sets the PWM Duty Cycle onto the pin controlling headlight / taillight brightness
 * @param brightness_pct  The duty cycle (brightness) to set on the PWM in percent
 */
static void lights_set_brightness(uint8_t brightness_pct);



/// The flag that indicates if lights should be toggled @ 1Hz
static bool lights_toggle_mode = false;

/// The brightness percentage that the lights should be configured for.
static uint8_t lights_set_brightness_pct = 0;

/// The time period that a brightness pct should persist for
static uint32_t lights_set_for_time_sec = 0;

/// Global instance (bad practice for classes but yolo). Freq is 1kHz so 1 period = 1 ms
static PWM lights_pwm(PWM::pwm1, 1000);



void lights_handle_lights_1Hz(void)
{
	// Highest priority: handle the manual light command if set
	if (lights_set_for_time_sec > 0)
	{
		lights_set_brightness(lights_set_brightness_pct);
		lights_set_for_time_sec--;
	}
	// Toggling gets next priority
	else if (lights_toggle_mode)
	{
		// Toggle the lights on / off
		lights_set_brightness_pct = lights_set_brightness_pct > 0 ? 0 : 100;
		lights_set_brightness(lights_set_brightness_pct);
	}
	else
	{
		lights_set_by_ambient_light();
	}
}

void lights_toggle_lights_1Hz(void)
{
	// Assert toggle flag and start duty cycle at 100 pct, or deassert if set.
	lights_toggle_mode = !lights_toggle_mode;
	lights_set_brightness_pct = 100;
}

void lights_set_brightness(uint8_t brightness_pct)
{
	lights_pwm.set(brightness_pct);
}

void lights_set_by_ambient_light(void)
{
	// Light sensor readings range from 0-100%; ie. we want our light PWM to be at 100% duty when outside light is 0%
	const uint8_t light_reading_pct = LS.getPercentValue();
	const uint8_t brightness = light_reading_pct < 25 ? 100 - light_reading_pct : 0;
	lights_set_brightness(brightness);
}

void lights_set_brightness_for_time(uint8_t brightness_pct, uint32_t time_sec)
{
	lights_set_brightness_pct = brightness_pct;
	lights_set_for_time_sec   = time_sec;
}

#endif // ECU_SENSOR




