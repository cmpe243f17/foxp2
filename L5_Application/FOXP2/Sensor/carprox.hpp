/*
 * CAN Interface
 * @author jtran
 * @version 1.0.0
 */

#ifndef CARPROX_HPP
#define CARPROX_HPP

#include <stdint.h>

typedef enum {
    CARPROX_FRONT=0,
    CARPROX_LFRONT,
    CARPROX_RFRONT,
    CARPROX_COUNT,
} CARPROXIndex_E;

void CARPROX_init(void);
void CARPROX_task_10hz(void);
void CARPROX_task_100hz(void);
uint32_t CARPROX_get_distance_in(CARPROXIndex_E index);

#endif  // CARPROX_HPP
