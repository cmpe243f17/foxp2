#ifndef MB1010_HPP
#define MB1010_HPP

#include <stdint.h>

#include "gpio.hpp"


class MB1010
{
public:
    MB1010(uint8_t adc_channel, LPC1758_GPIO_Type gpio);
    void init(void);
    void update_distance(void);
    uint32_t get_distance(void);
private:
    uint32_t distance_in;
    uint8_t adc_ch;
    GPIO *rx;
};


#endif  // MB1010_HPP
