#include "mb1010.hpp"

#include "adc0.h"
#include "lpc_timers.h"

#define DECODE_ADC_RAW_VALUE_IN(raw_value) ((raw_value <= 75) ? (6) : (raw_value / 12.5F))


MB1010::MB1010(uint8_t adc_channel, LPC1758_GPIO_Type gpio)
{
    this->adc_ch = adc_channel;
    // TODO: Initialize ADC pin here
    this->rx = new GPIO(gpio);
    this->distance_in = UINT32_MAX;
}


void MB1010::init(void)
{
    const lpc_timer_t timer = lpc_timer0;
    lpc_timer_enable(timer, 1U);  // 1us tick period
    lpc_timer_set_value(timer, 0U);
    this->rx->setAsOutput();
    this->rx->setHigh();
    while (lpc_timer_get_value(timer) < (1000U * 100U)) {
        ;
    }
    this->rx->setLow();
    this->rx->setAsInput();  // Stop driving the line
}


void MB1010::update_distance(void)
{
    uint32_t raw_value = (uint32_t)adc0_get_reading(this->adc_ch);
    this->distance_in = (uint32_t)DECODE_ADC_RAW_VALUE_IN(raw_value);
}


uint32_t MB1010::get_distance(void)
{
    return this->distance_in;
}
