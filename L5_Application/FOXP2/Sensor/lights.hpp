/*
 * lights.hpp
 *
 *  Created on: Nov 25, 2017
 *      Author: Taylor
 */

#ifndef L5_APPLICATION_FOXP2_LIGHTS_HPP_
#define L5_APPLICATION_FOXP2_LIGHTS_HPP_

/// Handles LED brightness based off of ambient light, commanded brightness, and/or toggling command
void lights_handle_lights_1Hz(void);

/// Toggles the lights off / on @ 1Hz; call this function to start toggling, and once again to stop toggling
void lights_toggle_lights_1Hz(void);

/**
 * Sets the PWM Duty Cycle onto the pin controlling headlight / taillight brightness for a given period
 * @param brightness_pct  The duty cycle (brightness) to set on the PWM in percent
 * @param time_ms         The time to keep LEDs on the brightness
 */
void lights_set_brightness_for_time(uint8_t brightness_pct, uint32_t time_sec);


#endif /* L5_APPLICATION_FOXP2_LIGHTS_HPP_ */
