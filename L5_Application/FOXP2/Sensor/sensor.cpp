#include "sensor.hpp"

#include <stdio.h>

#include "adc0.h"
#include "can_if.hpp"
#include "carprox.hpp"
#include "lights.hpp"

void sensor_init()
{
    CANIF_init();
    CARPROX_init();
}

void periodic_sensor_1HZ()
{
    CANIF_task_1hz();
//    printf("%d %d %d\n", adc0_get_reading(4), adc0_get_reading(3), adc0_get_reading(5));
}

void periodic_sensor_10HZ()
{
    CANIF_task_10hz();
    CARPROX_task_10hz();
}

void periodic_sensor_100HZ()
{
    CARPROX_task_100hz();
}

void periodic_sensor_1KHZ()
{

}

void sensor_low_priority_init(void)
{
	lights_toggle_lights_1Hz();
}

void low_priority_sensor_1Hz(unsigned int count)
{
	// We start toggling lights at 1Hz in the init--turn it off after 10 secs
	if (count == 10) {
		lights_toggle_lights_1Hz();
	}

	// Low priority checks go here (aka the fun stuff)
	lights_handle_lights_1Hz();

	// Override the periodic LED handling for 5 secs every min with an 'on' command for funsies
	if (0 == count % 60) {
		lights_set_brightness_for_time(100, 5);
	}
	//////////////////////////////////////////////////
}
