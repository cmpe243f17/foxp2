#ifndef SENSOR_HPP
#define SENSOR_HPP

void sensor_low_priority_init(void);
void low_priority_sensor_1Hz(unsigned int);
void sensor_init(void);
void periodic_sensor_1HZ(void);
void periodic_sensor_10HZ(void);
void periodic_sensor_100HZ(void);
void periodic_sensor_1KHZ(void);

#endif  // SENSOR_HPP
