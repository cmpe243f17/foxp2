#include "can_if.hpp"

#include <stdbool.h>
#include <stdint.h>

#include "can.h"
#include "carprox.hpp"
#include "generated_can.h"
#include "Build_Config.hpp"
#include "io.hpp"

// CAN
#define CANBUS1 can1
#define RXQ_SIZE 100U
#define TXQ_SIZE 100U

static void transmit_prox_status(void);

/*
 *  Public functions
 */
void CANIF_init(void)
{
    (void)CAN_init(CANBUS1, CAN_BAUDRATE, RXQ_SIZE, TXQ_SIZE, NULL, NULL);
    CAN_bypass_filter_accept_all_msgs();
    CAN_reset_bus(CANBUS1);
}

void CANIF_task_1hz(void)
{
    if (CAN_is_bus_off(CANBUS1)) {
        CAN_reset_bus(CANBUS1);
    }
}

void CANIF_task_10hz(void)
{
    if (!CAN_is_bus_off(CANBUS1)) {
        transmit_prox_status();
        LE.toggle(4U);
    }
    else {
        LE.off(4U);
    }
}

static void transmit_prox_status(void)
{
    SENSOR_PROX_STATUS_t status = {
        .SENSOR_FRONT_DIST = (uint16_t)CARPROX_get_distance_in(CARPROX_FRONT),
        .SENSOR_LFRONT_DIST = (uint16_t)CARPROX_get_distance_in(CARPROX_LFRONT),
        .SENSOR_RFRONT_DIST = (uint16_t)CARPROX_get_distance_in(CARPROX_RFRONT),
    };

    can_msg_t msg = {0};
    dbc_msg_hdr_t msg_hdr = dbc_encode_SENSOR_PROX_STATUS(msg.data.bytes, &status);
    msg.msg_id = msg_hdr.mid;
    msg.frame_fields.data_len = msg_hdr.dlc;
    (void)CAN_tx(CANBUS1, &msg, 0U);
}
