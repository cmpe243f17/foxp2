#include "IO_CAN_gateway_messageSpecific.h"

#ifdef ECU_GATEWAY

extern const IO_CAN_gateway_messageSpecific_data_S rxMessageSpecificData[RX_ID_COUNT];
const IO_CAN_gateway_messageSpecific_data_S rxMessageSpecificData[RX_ID_COUNT] =
{
	[RPM_VALUE_CMD] =
	{
		.idHex = 0x078, // 120 
		.decodeFctArray[RPM_VALUE_CMD] = (void*)dbc_decode_RPM_VALUE_CMD,
	},
	[GEOGRAPHICAL_CMD] =
	{
		.idHex = 0x0C8, // 200
		.decodeFctArray[GEOGRAPHICAL_CMD] = (void*)dbc_decode_GEOGRAPHICAL_CMD,
	},
	[SENSOR_PROX_STATUS] =
	{
		.idHex = 0x12C, // 300
		.decodeFctArray[SENSOR_PROX_STATUS] = (void*)dbc_decode_SENSOR_PROX_STATUS,
	},
};

extern const IO_CAN_gateway_messageSpecific_data_S txMessageSpecificData[TX_ID_COUNT];
const IO_CAN_gateway_messageSpecific_data_S txMessageSpecificData[TX_ID_COUNT] =
{
	[MOTOR_CMD] =
	{
		.idHex = 0x064, // 100
		.encodeFctArray[MOTOR_CMD] = (void*)dbc_encode_and_send_MOTOR_CMD,
	},
};

#endif  //GATEWAY
