#ifndef IO_CAN_GATEWAY_MESSAGESPECIFIC_H_
#define IO_CAN_GATEWAY_MESSAGESPECIFIC_H_


#include "generated_can.h"

#ifdef  ECU_GATEWAY

// *********************EXTERNS************************ //

extern GEOGRAPHICAL_CMD_t geo;
extern MOTOR_CMD_t motor;
extern SENSOR_PROX_STATUS_t sensor;
extern RPM_VALUE_CMD_t rpm;

// *********************ENUMS************************** //

/*
	Receive Messages
	USE THE DBC MESSAGE NAME!
*/
typedef enum 
{
	RPM_VALUE_CMD,
	GEOGRAPHICAL_CMD,		           
    SENSOR_PROX_STATUS,
    // UNUSED_RX_ID_4
    // UNUSED_RX_ID_5		
	RX_ID_COUNT,			

} IO_CAN_gateway_messageSpecific_rxID_E;

/*
	Transmit Messages
	USE THE DBC MESSAGE NAME!
*/
typedef enum 
{
	MOTOR_CMD,
	// UNUSED_TX_ID_1
	// UNUSED_TX_ID_2
	// UNUSED_TX_ID_3
	TX_ID_COUNT,

} IO_CAN_gateway_messageSpecific_txID_E;

// *********************UNIONS************************** //


// *********************STRUCTS************************** //

typedef struct 
{
	uint16_t idHex;
	bool (*decodeFctArray[RX_ID_COUNT]) (void* message_t, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr);
	bool (*encodeFctArray[TX_ID_COUNT]) (void* message_t);

} IO_CAN_gateway_messageSpecific_data_S;

#endif  // GATEWAY

#endif /* IO_CAN_GATEWAY_MESSAGESPECIFIC_H_ */