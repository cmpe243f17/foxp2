#ifndef IO_CAN_GATEWAY_H_
#define IO_CAN_GATEWAY_H_

#ifdef ECU_GATEWAY


#ifdef __cplusplus
extern "C" {
#endif

#include "can.h"
#include "../../Build_Config.hpp"
#include <stdint.h>
#include <stdio.h>
#include "printf_lib.h"
#include "IO_CAN_gateway_messageSpecific.h"

#define CHECK_IF_GROUP_OF_TWO(id) (id % 2)
#define CHECK_IF_LAST_RX_ID(id)   (1 == (RX_ID_COUNT - id))
#define DISABLED_ENTRY			  0xFFFF 
#define INDEX_MINUS_ONE(index)	  (index - 1)			  

typedef struct 
{
	can_std_id_t 		fullCANID; 		// full can id
	can_fullcan_msg_t 	canMessage;		// raw CAN message

} IO_gateway_messageData_S;

bool IO_gateway_init(void);
void IO_gateway_run_100ms(void);

#ifdef __cplusplus
}
#endif

#endif  //GATEWAY
#endif /* IO_CAN_GATEWAY_H_ */

