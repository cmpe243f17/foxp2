#include "IO_CAN_gateway_messageSpecific.h"

#ifdef  ECU_GATEWAY

//*******************EXTERN DEFINITIONS*********************//


const uint32_t GEOGRAPHICAL_CMD__MIA_MS = 3000;
const GEOGRAPHICAL_CMD_t GEOGRAPHICAL_CMD__MIA_MSG =
{
	.GEOGRAPHICAL_CMD_direction = 0x3,
	.GEOGRAPHICAL_CMD_distance = 50,
};

const uint32_t RPM_VALUE_CMD__MIA_MS = 3000;
const RPM_VALUE_CMD_t RPM_VALUE_CMD__MIA_MSG = 
{
	.RPM_VALUE_CMD_WHEEL_CLICKS = 1000,
};

const uint32_t SENSOR_PROX_STATUS__MIA_MS = 1000;
const SENSOR_PROX_STATUS_t SENSOR_PROX_STATUS__MIA_MSG =
{
	.SENSOR_FRONT_DIST = 0,
	.SENSOR_RFRONT_DIST = 0,
	.SENSOR_LFRONT_DIST = 0,
	.SENSOR_REAR_DIST = 0,
};

//*******************MIA HANDLING**************************//

static void IO_CAN_gateway_messageMia_GEO(void)
{
	if (dbc_handle_mia_GEOGRAPHICAL_CMD(&geo, 100))
	{
		// do something when the geo controller goes out to lunch
	}
}

static void IO_CAN_gateway_messageMia_RPM(void)
{
	if (dbc_handle_mia_RPM_VALUE_CMD(&rpm, 100))
	{

	}
}

static void IO_CAN_gateway_messageMia_SENSOR(void)
{
	if (dbc_handle_mia_SENSOR_PROX_STATUS(&sensor, 100))
	{
		// do something when the sensor controller goes out to lunch
	}
}

void IO_CAN_gateway_messageMia_run_100ms(void)
{
	IO_CAN_gateway_messageMia_GEO();
	IO_CAN_gateway_messageMia_SENSOR();
	IO_CAN_gateway_messageMia_RPM();
}

#endif