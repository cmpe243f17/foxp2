#include "IO_CAN_gateway.h"
#include "IO_CAN_gateway_messageSpecific.h"
#include "IO_CAN_gateway_messageMia.h"
#include "string.h"

#ifdef ECU_GATEWAY

IO_gateway_messageData_S messageData[RX_ID_COUNT];
const IO_CAN_gateway_messageSpecific_data_S rxMessageSpecificData[RX_ID_COUNT];
const IO_CAN_gateway_messageSpecific_data_S txMessageSpecificData[TX_ID_COUNT];

// **************************** EXTERN DEFINITIONS ********************************//

// Receive Handles
GEOGRAPHICAL_CMD_t geo = {0};
SENSOR_PROX_STATUS_t sensor = {0};
RPM_VALUE_CMD_t rpm = {0};

// Transmit Handles
MOTOR_CMD_t motor = {0};

// Receive getters
void* IO_gateway_get_GEOGRAPHICAL_CMD_handle(void)
{
	return &geo;
}

void* IO_gateway_get_SENSOR_PROX_STATUS_handle(void)
{
	return &sensor;
}

void* IO_gateway_get_RPM_VALUE_CMD_handle(void)
{
	return &rpm;
}

// Transmit getters
void* IO_gateway_get_MOTOR_CMD_handle(void)
{
	return &motor;
}


// array of function pointers pointing to RX getter functions
void* (*rxMsgHandles[RX_ID_COUNT])(void) =
{
	IO_gateway_get_RPM_VALUE_CMD_handle,
 	IO_gateway_get_GEOGRAPHICAL_CMD_handle, 
  	IO_gateway_get_SENSOR_PROX_STATUS_handle, 
};

// array of function pointers pointing to TX getter functions 
void* (*txMsgHandles[TX_ID_COUNT])(void) =
{
	IO_gateway_get_MOTOR_CMD_handle
};

// ********************************************************************************//

/*
	FullCAN initialization:

	1. Initialize the CAN peripheral

	2. Generate FullCAN IDs by passing the HEX ID of the CAN message to CAN_gen_sid()
	   
	   *NOTE*: Messages must be added starting from the lowest CAN ID

	3. Add an entry to FullCANs message acceptance filter by passing the generated FullCAN ID from step 2. 

	   *NOTE*: Messages must be added in groups of 2 entries; if there are an odd numbered of messages,
	   		   then a DISABLED entry ID must be generated to take place of the second entry.

	4. Reset the CAN peripheral (enable bus) AFTER all messages have been added to FullCANs
	   message filter.

*/

bool IO_gateway_init(void)
{
	bool success = false;

	//TODO: Add callbacks for when bus enters error state and encounters overrun.
	//		Ideally, the callbacks should put a message on CAN about these events

	// Initialize the CAN peripheral
	success = CAN_init(can1, CAN_BAUDRATE, RECEIVER_QUEUE_SIZE, SENDER_QUEUE_SIZE, NULL, NULL);

	// Add the entries so that FullCAN knows what messages to accept. This will also enable FullCAN to start storing these messages in RAM.
	for (IO_CAN_gateway_messageSpecific_rxID_E id = (IO_CAN_gateway_messageSpecific_rxID_E)0; id < RX_ID_COUNT; id++)
	{
		// Generate FullCAN ID and store it -- *CAN IDS NEED TO BE ADDED STARTING FROM LEAST TO GREATEST*
		messageData[id].fullCANID = CAN_gen_sid(can1, rxMessageSpecificData[id].idHex);

		if (CHECK_IF_GROUP_OF_TWO(id)) // FullCAN API requires that we add messages in groups of 2; id is 0 indexed
		{
			success = CAN_fullcan_add_entry(can1, messageData[INDEX_MINUS_ONE(id)].fullCANID, messageData[id].fullCANID);
		}
		else if (CHECK_IF_LAST_RX_ID(id)) // it's possible that we might have an odd # of total messages in total 
		{
			can_std_id_t disabledEntryID = CAN_gen_sid(can1, DISABLED_ENTRY);
			success = CAN_fullcan_add_entry(can1, messageData[id].fullCANID, disabledEntryID);
		}
	}

	// Reset CAN peripheral
	CAN_reset_bus(can1);

	return success;
}

/*
	Steps to fetching a message that has been received and stored into RAM (courtesy of FullCAN):

		1. Get the pointer to where the actual CAN message is stored in memory.

		   This is accomplished by passing the FullCAN entry IDs WE initialized 
		   for all messages we want FullCAN to receive
		
		2. Copy the data into a can_fullcan_msg_t. 

		   This is accomplished by passing the pointer we fetched in step 1.

		3. Pass data bytes to generated decode function for corresponding mID
*/

static void IO_gateway_readAndDecode(void)
{
	dbc_msg_hdr_t can_msg_hdr;

	// update the copy of every CAN message stored in RAM 	
	for (IO_CAN_gateway_messageSpecific_rxID_E id = (IO_CAN_gateway_messageSpecific_rxID_E)0; id < RX_ID_COUNT; id++)
	{
		// get ptr to where message is stored in RAM 
		can_fullcan_msg_t *entryPtr = CAN_fullcan_get_entry_ptr(messageData[id].fullCANID);

		// check if HW received a new message & update the local copy
		bool newMessageArrived = CAN_fullcan_read_msg_copy(entryPtr, &messageData[id].canMessage);

		// Decode if new message
		if (newMessageArrived)
		{
			can_msg_hdr.dlc = messageData[id].canMessage.data_len;
        	can_msg_hdr.mid = messageData[id].canMessage.msg_id;

    		// get the decode function from the decode function array using the current index
        	rxMessageSpecificData[id].decodeFctArray[id](rxMsgHandles[id](), messageData[id].canMessage.data.bytes, &can_msg_hdr);		
			// printf("received: %i", id);
		}
		else
		{
			//HW didn't get a new message because the node is not on the bus? --> would eventually get to MIA handling
			//HW didn't get a new message because of dropped messages? --> missedMessageCount? 
		}
	}
}

static void IO_gateway_encodeAndSend(void)
{
	for (IO_CAN_gateway_messageSpecific_txID_E id = (IO_CAN_gateway_messageSpecific_txID_E)0; id < TX_ID_COUNT; id++)
	{
		// get the encode function from the encode function array using the current index
		txMessageSpecificData[id].encodeFctArray[id](txMsgHandles[id]());
		// printf("sent: %i", id);
	}
}

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);

    return CAN_tx(can1, &can_msg, 0);
}

void IO_gateway_run_100ms(void)
{
	IO_gateway_readAndDecode();
	IO_gateway_encodeAndSend();
}

#endif  //GATEWAY
