#ifndef IO_CAN_GATEWAY_MESSAGEMIA_H_
#define IO_CAN_GATEWAY_MESSAGEMIA_H_
#ifdef  ECU_GATEWAY


#ifdef __cplusplus
extern "C" {
#endif
void IO_CAN_gateway_messageMia_init(void);
void IO_CAN_gateway_messageMia_run_100ms(void);
#ifdef __cplusplus
}
#endif

#endif  //GATEWAY
#endif