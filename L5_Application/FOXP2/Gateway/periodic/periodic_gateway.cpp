#include "../IO_CAN/IO_CAN_gateway.h"
#include "../IO_CAN/IO_CAN_gateway_messageMia.h"
#include "../app/app_object_detection.h"
#include "io.hpp"


#ifdef ECU_GATEWAY

void gateway_init(void)
{
	if (IO_gateway_init())
	{
		printf("init failed");
	}

	app_object_detection_init();
}

void periodic_gateway_1HZ(void) 
{
	if (CAN_is_bus_off(can1))
	{
		LE.toggle(1);
		CAN_reset_bus(can1);
	}

	// u0_dbg_printf("front: (%i)\n", sensor.SENSOR_FRONT_DIST);
	// u0_dbg_printf("left: (%i)\n", sensor.SENSOR_LFRONT_DIST);
	// u0_dbg_printf("right: (%i)\n", sensor.SENSOR_RFRONT_DIST);
}

void periodic_gateway_10HZ(void)
{
	IO_CAN_gateway_messageMia_run_100ms();
}

void periodic_gateway_100HZ(void)
{
	static uint16_t counter = 0;
	static bool initDelay = false; 
	counter+=1; 
	if(counter >= 10)
	{
		initDelay = true;
	}

	if (initDelay)
	{
		IO_gateway_run_100ms();
		app_object_detection_run10ms();	

		LD.setNumber(app_object_detection_getState());

		
		if(SW.getSwitch(1))
		{	
			app_object_detection_enable_GPS_control(true);
			LE.on(4);
		}

		if(SW.getSwitch(2))
		{
			app_object_detection_enable_GPS_control(false);
			LE.off(4);
		}
	}

	if (app_object_detection_isGPSInControl())
	{
		// GPS NOT in control
		LE.on(2);
	}
	else
	{
		// GPS in 
		LE.off(2);
	}
}

void periodic_gateway_1KHZ(void)
{

}

void gateway_low_priority_init(void)
{

}

void low_priority_gateway_1Hz(unsigned int cnt)
{

}

#endif 
