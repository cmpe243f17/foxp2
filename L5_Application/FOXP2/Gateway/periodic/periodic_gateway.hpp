#ifndef PERIODIC_GATEWAY_H_
#define PERIODIC_GATEWAY_H_
#ifdef __cplusplus
extern "C" {
#endif



#ifdef __cplusplus
}

void gateway_init(void);
void periodic_gateway_1HZ(void);
void periodic_gateway_10HZ(void);
void periodic_gateway_100HZ(void);
void periodic_gateway_1KHZ(void);

void gateway_low_priority_init(void);
void low_priority_gateway_1Hz(unsigned int count);

#endif
#endif /* PERIODIC_GATEWAY_H_ */
