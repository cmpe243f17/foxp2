#ifndef APP_OBJECT_DETECTION_H
#define APP_OBJECT_DETECTION_H

#ifdef ECU_GATEWAY

#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "stdbool.h"

#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

typedef enum 
{
	APP_OBJECT_DETECTION_STATE_UNINIT,
	APP_OBJECT_DETECTION_STATE_INIT,
	APP_OBJECT_DETECTION_STATE_ALL_CLEAR,		// all sensors are clear
	APP_OBJECT_DETECTION_STATE_ALL_BLOCKED,		// all sensors are blocked
	APP_OBJECT_DETECTION_STATE_L_BLOCKED,       // left sensor is blocked
	APP_OBJECT_DETECTION_STATE_L_R_BLOCKED,     // left and right sensors are blocked
	APP_OBJECT_DETECTION_STATE_L_F_BLOCKED,     // left and front sensors are blocked
	APP_OBJECT_DETECTION_STATE_R_BLOCKED,       // right sensor is blocked
	APP_OBJECT_DETECTION_STATE_R_F_BLOCKED,     // right and front sensors are blocked 
	APP_OBJECT_DETECTION_STATE_FRONT_BLOCKED,
	APP_OBJECT_DETECTION_STATE_REAR_BLOCKED,    // rear sensor is blocked
	APP_OBJECT_DETECTION_STATE_READY,
	APP_OBJECT_DETECTION_STATE_FAULTED,

	APP_OBJECT_DETECTION_STATE_COUNT,
} app_object_detection_state_E;

typedef enum 
{
	APP_OBJECT_DETECTION_SENSOR_LEFT,
	APP_OBJECT_DETECTION_SENSOR_RIGHT,
	APP_OBJECT_DETECTION_SENSOR_FRONT,
	APP_OBJECT_DETECTION_SENSOR_REAR,
} app_object_detection_sensor_E;

typedef struct 
{
	app_object_detection_state_E state;
	bool isGPSActive;
	bool reverseRequest;
	bool ignoreGPS;
} app_object_detection_data_S;


typedef struct 
{
	const uint8_t sensorThreshInInches;
	const uint8_t frontSensorThreshInInches;
	const uint8_t leftSensorThreshInInches;
	const uint8_t rightSensorThreshInInches;
	const uint8_t rearSensorThreshInInches;

} app_object_detection_config_S;


#ifdef __cplusplus
extern "C" {
#endif
	void app_object_detection_init(void);
	void app_object_detection_run10ms(void);
	app_object_detection_state_E app_object_detection_getState(void);
	void app_object_detection_enable_GPS_control(bool state);
	bool app_object_detection_isGPSInControl(void);
	bool app_object_detection_isInitialized(void);
#ifdef __cplusplus
}
#endif

#endif
#endif