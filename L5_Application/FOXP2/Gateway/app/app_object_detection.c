#include "app_object_detection.h"
#include "../IO_CAN/IO_CAN_gateway_messageSpecific.h"
#include "../IO_CAN/IO_CAN_gateway.h"
#include "app_object_detection_componentSpecific.h"
#include "Build_Config.hpp"
#include "FreeRTOS.h"
#include "task.h"

#ifdef ECU_GATEWAY

extern const app_object_detection_config_S app_object_detection_config;
app_object_detection_data_S app_object_detection_data;

static void app_object_detection_determineMotorParameters(void);
static void app_object_detection_setMotorParameters(const momentum_E momentum, const direction_E direction);
static uint8_t app_object_detection_getSensorData(const app_object_detection_sensor_E physicalSensor);
static void app_object_detection_processGPSRequests(void);

static app_object_detection_state_E app_object_detection_getDesiredState(const app_object_detection_state_E currentState);
static void app_object_detection_entryAction(const app_object_detection_state_E nextState);
static void app_object_detection_runAction(const app_object_detection_state_E currentState);
static void app_object_detection_exitAction(const app_object_detection_state_E currentState, const app_object_detection_state_E nextState); 

static app_object_detection_state_E app_object_detection_getDesiredState(const app_object_detection_state_E currentState)
{
	app_object_detection_state_E state = currentState;

	if (app_object_detection_data.state == APP_OBJECT_DETECTION_STATE_UNINIT)
	{
		state = APP_OBJECT_DETECTION_STATE_INIT;
	}
	else if ((app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_LEFT) >= app_object_detection_config.sensorThreshInInches) && 
			 (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_RIGHT) >= app_object_detection_config.sensorThreshInInches) && 
			 (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_FRONT) >= app_object_detection_config.sensorThreshInInches))
	{
		state = APP_OBJECT_DETECTION_STATE_ALL_CLEAR;
	}
	else if ((app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_LEFT) < app_object_detection_config.sensorThreshInInches) && 
		     (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_RIGHT)< app_object_detection_config.sensorThreshInInches) && 
		     (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_FRONT) < app_object_detection_config.sensorThreshInInches))
	{
		state = APP_OBJECT_DETECTION_STATE_ALL_BLOCKED;
	}
	else if ((app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_LEFT) < app_object_detection_config.sensorThreshInInches) && 
		     (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_RIGHT) >= app_object_detection_config.sensorThreshInInches) && 
		     (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_FRONT) >= app_object_detection_config.sensorThreshInInches))
	{
		state = APP_OBJECT_DETECTION_STATE_L_BLOCKED;
	}
	else if ((app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_LEFT) >= app_object_detection_config.sensorThreshInInches) && 
			 (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_RIGHT) < app_object_detection_config.sensorThreshInInches) && 
			 (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_FRONT) >= app_object_detection_config.sensorThreshInInches))
	{
		state = APP_OBJECT_DETECTION_STATE_R_BLOCKED;
	}
	else if ((app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_LEFT) < app_object_detection_config.sensorThreshInInches) &&
			 (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_RIGHT) < app_object_detection_config.sensorThreshInInches) &&
		     (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_FRONT) >= app_object_detection_config.sensorThreshInInches)) 	     
	{
		state = APP_OBJECT_DETECTION_STATE_L_R_BLOCKED;
	}
	else if ((app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_LEFT) < app_object_detection_config.sensorThreshInInches) && 
			 (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_RIGHT) >= app_object_detection_config.sensorThreshInInches) &&
		     (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_FRONT) < app_object_detection_config.frontSensorThreshInInches))
	{
		state = APP_OBJECT_DETECTION_STATE_L_F_BLOCKED;
	}
	else if ((app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_LEFT) >= app_object_detection_config.sensorThreshInInches) && 
		     (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_RIGHT) < app_object_detection_config.sensorThreshInInches) &&
		     (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_FRONT) < app_object_detection_config.frontSensorThreshInInches))
	{
		state = APP_OBJECT_DETECTION_STATE_R_F_BLOCKED;
	}
	else if ((app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_LEFT) >= app_object_detection_config.sensorThreshInInches) && 
		     (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_RIGHT) > app_object_detection_config.sensorThreshInInches) &&
		     (app_object_detection_getSensorData(APP_OBJECT_DETECTION_SENSOR_FRONT) < app_object_detection_config.sensorThreshInInches))
	{
		state = APP_OBJECT_DETECTION_STATE_FRONT_BLOCKED;
	}
	else 
	{
		// state = APP_OBJECT_DETECTION_STATE_FRONT_BLOCKED;
	}

	return state;
}

static void app_object_detection_entryAction(const app_object_detection_state_E nextState)
{

}

static void app_object_detection_runAction(const app_object_detection_state_E currentState)
{
	if (CHECK_BIT(geo.GEOGRAPHICAL_CMD_direction, GEO_STOP))
	{
		app_object_detection_setMotorParameters(MOMENTUM_STOP, TURN_STRAIGHT);
	}	
	else if (app_object_detection_data.reverseRequest == true)
	{
		static uint8_t reverseCounter = 0;
		reverseCounter +=1; 
		if (reverseCounter >= 100)
		{
			app_object_detection_data.reverseRequest = false;
			app_object_detection_data.ignoreGPS = true;
			reverseCounter = 0;
		}
		else
		{
			switch(app_object_detection_data.state)
			{
				case APP_OBJECT_DETECTION_STATE_L_F_BLOCKED:
					app_object_detection_setMotorParameters(MOMENTUM_REVERSE, TURN_LEFT);
					break;
				case APP_OBJECT_DETECTION_STATE_R_F_BLOCKED:
					app_object_detection_setMotorParameters(MOMENTUM_REVERSE, TURN_RIGHT);
					break;
				case APP_OBJECT_DETECTION_STATE_L_R_BLOCKED:
					app_object_detection_setMotorParameters(MOMENTUM_REVERSE, TURN_RIGHT);
					break;
				case APP_OBJECT_DETECTION_STATE_FRONT_BLOCKED:
					app_object_detection_setMotorParameters(MOMENTUM_REVERSE, TURN_RIGHT);
					break;
				case APP_OBJECT_DETECTION_STATE_ALL_BLOCKED:
					app_object_detection_setMotorParameters(MOMENTUM_REVERSE, TURN_RIGHT);
					break;
			}
			
		}
	}
	else if(app_object_detection_data.ignoreGPS == true)
	{
		static uint16_t ignoreGPSCounter = 0;
		ignoreGPSCounter += 1;
		if (ignoreGPSCounter >= 300)
		{
			app_object_detection_data.ignoreGPS = false;
			ignoreGPSCounter = 0;
		}
		else
		{
			app_object_detection_determineMotorParameters();
		}
	}
	else
	{
		app_object_detection_determineMotorParameters();
	}

}

bool app_object_detection_isGPSInControl(void)
{
	return app_object_detection_data.ignoreGPS;
}

static void app_object_detection_exitAction(const app_object_detection_state_E currentState, const app_object_detection_state_E nextState)
{

}

static void app_object_detection_processGPSRequests(void)
{
		if (CHECK_BIT(geo.GEOGRAPHICAL_CMD_direction, GEO_LEFT))
		{
			app_object_detection_setMotorParameters(MOMENTUM_FORWARD, TURN_LEFT);
		}
		else if (CHECK_BIT(geo.GEOGRAPHICAL_CMD_direction, GEO_RIGHT))
		{
			app_object_detection_setMotorParameters(MOMENTUM_FORWARD, TURN_RIGHT);
		}
		else if (CHECK_BIT(geo.GEOGRAPHICAL_CMD_direction, GEO_STRAIGHT))
		{
			app_object_detection_setMotorParameters(MOMENTUM_FORWARD, TURN_STRAIGHT);
		}
		else if (CHECK_BIT(geo.GEOGRAPHICAL_CMD_direction, GEO_STOP))
		{
			app_object_detection_setMotorParameters(MOMENTUM_STOP, TURN_STRAIGHT);
		}
		else
		{
			// motor.MOTOR_CMD_MOMENTUM = MOMENTUM_STOP;
			// motor.MOTOR_CMD_TURN     = TURN_STRAIGHT;
		}
}

static void app_object_detection_determineMotorParameters(void)
{
	switch(app_object_detection_data.state)
	{
		case APP_OBJECT_DETECTION_STATE_UNINIT:
		case APP_OBJECT_DETECTION_STATE_INIT:
			app_object_detection_setMotorParameters(MOMENTUM_STOP, TURN_STRAIGHT);
			app_object_detection_data.reverseRequest = true;
			break;
		case APP_OBJECT_DETECTION_STATE_ALL_CLEAR:
			if((app_object_detection_data.isGPSActive == true) &&
			   (app_object_detection_data.ignoreGPS == false))
			{
				app_object_detection_processGPSRequests();
			}
			else
			{
				app_object_detection_setMotorParameters(MOMENTUM_FORWARD, TURN_STRAIGHT);
			}
			break;
		case APP_OBJECT_DETECTION_STATE_ALL_BLOCKED:
			app_object_detection_setMotorParameters(MOMENTUM_STOP, TURN_STRAIGHT);
			app_object_detection_data.reverseRequest = true;
			break;
		case APP_OBJECT_DETECTION_STATE_L_BLOCKED:
			app_object_detection_setMotorParameters(MOMENTUM_FORWARD, TURN_RIGHT);
			break;
		case APP_OBJECT_DETECTION_STATE_L_R_BLOCKED:
			app_object_detection_setMotorParameters(MOMENTUM_STOP, TURN_STRAIGHT);
			app_object_detection_data.reverseRequest = true;
			break;
		case APP_OBJECT_DETECTION_STATE_L_F_BLOCKED:
			app_object_detection_setMotorParameters(MOMENTUM_STOP, TURN_STRAIGHT);
			app_object_detection_data.reverseRequest = true;
			break;
		case APP_OBJECT_DETECTION_STATE_R_BLOCKED:
			app_object_detection_setMotorParameters(MOMENTUM_FORWARD, TURN_LEFT);
			break;
		case APP_OBJECT_DETECTION_STATE_R_F_BLOCKED:
			app_object_detection_setMotorParameters(MOMENTUM_STOP, TURN_STRAIGHT);
			app_object_detection_data.reverseRequest = true;
			break;
		case APP_OBJECT_DETECTION_STATE_FRONT_BLOCKED:
			app_object_detection_setMotorParameters(MOMENTUM_STOP, TURN_STRAIGHT);
			app_object_detection_data.reverseRequest = true;
			break;
		case APP_OBJECT_DETECTION_STATE_REAR_BLOCKED:
			// app_object_detection_setMotorParameters(MOMENTUM_STOP, TURN_STRAIGHT);
			break;

		case APP_OBJECT_DETECTION_STATE_READY:
			// break;
		case APP_OBJECT_DETECTION_STATE_FAULTED:
		break;
			// app_object_detection_setMotorParameters(MOMENTUM_STOP, TURN_STRAIGHT);
			// break;
	}
}

static void app_object_detection_setMotorParameters(const momentum_E momentum, const direction_E direction)
{
	taskENTER_CRITICAL();
	motor.MOTOR_CMD_MOMENTUM    = momentum;
	motor.MOTOR_CMD_TURN        = direction;
	taskEXIT_CRITICAL();
}

static uint8_t app_object_detection_getSensorData(const app_object_detection_sensor_E physicalSensor)
{
	uint8_t sensorReading = 0;

	switch(physicalSensor)
	{
		case APP_OBJECT_DETECTION_SENSOR_LEFT:
			sensorReading = sensor.SENSOR_LFRONT_DIST;
			break;
		case APP_OBJECT_DETECTION_SENSOR_RIGHT:
			sensorReading = sensor.SENSOR_RFRONT_DIST;
			break;
		case APP_OBJECT_DETECTION_SENSOR_FRONT:
			sensorReading = sensor.SENSOR_FRONT_DIST;
			break;
		case APP_OBJECT_DETECTION_SENSOR_REAR:
			sensorReading = sensor.SENSOR_REAR_DIST;
			break;
	}

	return sensorReading;
}

void app_object_detection_init(void)
{
	app_object_detection_data.state = APP_OBJECT_DETECTION_STATE_UNINIT;
	app_object_detection_data.reverseRequest = false;
	app_object_detection_data.ignoreGPS = false;
	app_object_detection_data.isGPSActive = true;
}

void app_object_detection_run10ms(void)
{
	const app_object_detection_state_E desiredState = app_object_detection_getDesiredState(app_object_detection_data.state);

	if (desiredState != app_object_detection_data.state)
	{
		app_object_detection_exitAction(app_object_detection_data.state, desiredState);
		app_object_detection_entryAction(desiredState);	

		app_object_detection_data.state = desiredState;
	}

	app_object_detection_runAction(desiredState);
}

app_object_detection_state_E app_object_detection_getState(void)
{
	return app_object_detection_data.state;
}

void app_object_detection_enable_GPS_control(bool state)
{
	app_object_detection_data.isGPSActive = state;
}

#endif