#include "app_object_detection.h"

extern const app_object_detection_config_S app_object_detection_config;
const app_object_detection_config_S app_object_detection_config = 
{
	.sensorThreshInInches = 10,
	.frontSensorThreshInInches = 10,
	.leftSensorThreshInInches = 12, //sides pick up first
	.rightSensorThreshInInches = 12,
	.rearSensorThreshInInches = 10,
};