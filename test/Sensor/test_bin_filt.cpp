#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

#include <stdio.h>
#include <cstdint>

#include "bin_filt.hpp"

using namespace cgreen;


Ensure(tc0)  // Test getting minimum and maximum
{
    BinFilt binfilt(5);
    binfilt.insert(INT32_MAX);
    binfilt.insert(INT32_MIN);
    binfilt.insert(0);
    assert_that(binfilt.get_max(), is_equal_to(INT32_MAX));
    assert_that(binfilt.get_min(), is_equal_to(INT32_MIN));
}


Ensure(tc1)  // Test FIFO behavior
{
    BinFilt binfilt(3);
    binfilt.insert(INT32_MAX);
    binfilt.insert(INT32_MIN);
    binfilt.insert(INT32_MIN);
    binfilt.insert(INT32_MIN);
    assert_that(binfilt.get_max(), is_equal_to(INT32_MIN));
}


Ensure(tc2)  // Test default return values for getting minimum and maximum
{
    // Attempting to get maximum should saturate to minimum if no valid values
    // Attempting to get minimum should saturate to maximum if no valid values
    BinFilt binfilt(10);
    assert_that(binfilt.get_max(), is_equal_to(INT32_MIN));
    assert_that(binfilt.get_min(), is_equal_to(INT32_MAX));
}


Ensure(tc3)  // Test initial array values shall not affect actual inserted values
{
    BinFilt binfilt(10);  // Bin (array) should contain all 0s initially
    binfilt.insert(1);
    assert_that(binfilt.get_min(), is_equal_to(1));
}


TestSuite* test_bin_filt(void)
{
    TestSuite *new_suite = create_test_suite();
    add_test(new_suite, tc0);
    add_test(new_suite, tc1);
    add_test(new_suite, tc2);
    add_test(new_suite, tc3);
    return new_suite;
}



int main(void)
{
    TestSuite *suite = create_test_suite();
    add_suite(suite, test_bin_filt());
    int ret = run_test_suite(suite, create_text_reporter());
    return ret;
}
