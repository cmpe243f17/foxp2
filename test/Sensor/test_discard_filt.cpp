#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

#include <stdio.h>
#include <cstdint>

#include "discard_filt.hpp"

using namespace cgreen;


Ensure(tc0)  // Test discard at most 1 invalid value
{
    DiscardFilt discardfilt(1);
    assert_that(discardfilt.check_gte(INT32_MIN, 0), is_equal_to(false));
    assert_that(discardfilt.check_gte(INT32_MIN, 0), is_equal_to(true));
}


Ensure(tc1) // Test discard more than 1 invalid value
{
    DiscardFilt discardfilt(3);
    assert_that(discardfilt.check_gte(INT32_MIN, 0), is_equal_to(false));
    assert_that(discardfilt.check_gte(INT32_MIN, 0), is_equal_to(false));
    assert_that(discardfilt.check_gte(INT32_MIN, 0), is_equal_to(false));
    assert_that(discardfilt.check_gte(INT32_MIN, 0), is_equal_to(true));
}


Ensure(tc2) // Test discard counter resets
{
    DiscardFilt discardfilt(2);
    assert_that(discardfilt.check_gte(INT32_MIN, 0), is_equal_to(false));
    assert_that(discardfilt.check_gte(INT32_MIN, 0), is_equal_to(false));
    assert_that(discardfilt.check_gte(INT32_MIN, 0), is_equal_to(true));
    assert_that(discardfilt.check_gte(INT32_MIN, 0), is_equal_to(false));
    assert_that(discardfilt.check_gte(INT32_MIN, 0), is_equal_to(false));
    assert_that(discardfilt.check_gte(INT32_MIN, 0), is_equal_to(true));
}


Ensure(tc3) // Test no discard
{
    DiscardFilt discardfilt(2);
    assert_that(discardfilt.check_gte(INT32_MAX, 0), is_equal_to(true));
    assert_that(discardfilt.check_gte(INT32_MAX, 0), is_equal_to(true));
    assert_that(discardfilt.check_gte(INT32_MAX, 0), is_equal_to(true));
}


Ensure(tc4) // Test check gte (greater than or equal to)
{
    DiscardFilt discardfilt(UINT32_MAX);
    assert_that(discardfilt.check_gte(INT32_MAX, INT32_MAX-1), is_equal_to(true));
    assert_that(discardfilt.check_gte(INT32_MIN+1, INT32_MIN), is_equal_to(true));
    assert_that(discardfilt.check_gte(INT32_MAX-1, INT32_MAX), is_equal_to(false));
    assert_that(discardfilt.check_gte(INT32_MIN, INT32_MIN+1), is_equal_to(false));
}


Ensure(tc5)  // Test a series of invalid values followed by a series of valid values
{
    DiscardFilt discardfilt(1U);  // Discard up to one time
    assert_that(discardfilt.check_gte(0, INT32_MAX), is_equal_to(false));
    assert_that(discardfilt.check_gte(0, INT32_MAX), is_equal_to(true));
    assert_that(discardfilt.check_gte(0, INT32_MAX), is_equal_to(false));
    assert_that(discardfilt.check_gte(0, INT32_MAX), is_equal_to(true));
    assert_that(discardfilt.check_gte(0, INT32_MAX), is_equal_to(false));
    assert_that(discardfilt.check_gte(0, INT32_MAX), is_equal_to(true));

    assert_that(discardfilt.check_gte(INT32_MAX, 0), is_equal_to(true));
    assert_that(discardfilt.check_gte(INT32_MAX, 0), is_equal_to(true));
    assert_that(discardfilt.check_gte(INT32_MAX, 0), is_equal_to(true));
}


TestSuite* test_discard_filt(void)
{
    TestSuite *new_suite = create_test_suite();
    add_test(new_suite, tc0);
    add_test(new_suite, tc1);
    add_test(new_suite, tc2);
    add_test(new_suite, tc3);
    add_test(new_suite, tc4);
    return new_suite;
}


int main(void)
{
    TestSuite *suite = create_test_suite();
    add_suite(suite, test_discard_filt());
    int ret = run_test_suite(suite, create_text_reporter());
    return ret;
}
