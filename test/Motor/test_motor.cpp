#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

// Include for C++
using namespace cgreen;

#include "Motor_inc_private.hpp"
#include "Motor_ESC_Reverse_WA.cpp"
#include "Motor_LowLevel_Driver.cpp"
#include "Motor_RPM_Sensor.cpp"
#include "Motor_LCD.cpp"

// LCD UTs
Ensure(LCD_speed_check)
{
    assert_equal(send_data(SPEED_OBJ,0, -1000), false);
    assert_equal(send_data(SPEED_OBJ,0, -1), false);
    assert_equal(send_data(SPEED_OBJ,0, 0), true);
    assert_equal(send_data(SPEED_OBJ,0, 1), true);
    assert_equal(send_data(SPEED_OBJ,0, 49), true);
    assert_equal(send_data(SPEED_OBJ,0, 50), true);
    assert_equal(send_data(SPEED_OBJ,0, 51), false);
    assert_equal(send_data(SPEED_OBJ,0, 1000), false);
}

Ensure(LCD_gps_check)
{
    assert_equal(send_bearing_to_LCD(LCD_BEARING_TEXT, 0, 1), true);
    assert_equal(send_bearing_to_LCD(LCD_BEARING_TEXT, 360-1, 1), true);
    assert_equal(send_bearing_to_LCD(LCD_BEARING_TEXT, 360, 1), true);
    assert_equal(send_bearing_to_LCD(LCD_BEARING_TEXT, 360+1, 1), false);
    assert_equal(send_bearing_to_LCD(LCD_HEADING_TEXT, 0, 1), true);
    assert_equal(send_bearing_to_LCD(LCD_HEADING_TEXT, 360-1, 1), true);
    assert_equal(send_bearing_to_LCD(LCD_HEADING_TEXT, 360, 1), true);
    assert_equal(send_bearing_to_LCD(LCD_HEADING_TEXT, 360+1, 1), false);
}

// Motor UTs

Ensure(basic_state_machine_setting_check)
{    
    assert_equal(get_reverse_state(), NO_REVERSE);
    set_reverse_state(REVERSE_REQUESTED);
    assert_equal(get_reverse_state(), REVERSE_REQUESTED);
    set_reverse_state(FIRST_IDLE);
    assert_equal(get_reverse_state(), FIRST_IDLE);
    set_reverse_state(FIRST_REVERSE);
    assert_equal(get_reverse_state(), FIRST_REVERSE);
    set_reverse_state(SECOND_IDLE);
    assert_equal(get_reverse_state(), SECOND_IDLE);
    set_reverse_state(SECOND_REVERSE);
    assert_equal(get_reverse_state(), SECOND_REVERSE);
    set_reverse_state(REVERSE_DONE);
    assert_equal(get_reverse_state(), REVERSE_DONE);
}


Ensure(runtime_check)
{    
    reset_reverse_state();
    assert_equal(get_reverse_state(), NO_REVERSE);
    request_reverse();
    assert_equal(get_reverse_state(), REVERSE_REQUESTED);
    motor_drive_reverse();
    assert_equal(get_reverse_state(), FIRST_IDLE);
    motor_drive_reverse();
    assert_equal(get_reverse_state(), FIRST_REVERSE);
    motor_drive_reverse();
    assert_equal(get_reverse_state(), SECOND_IDLE);
    motor_drive_reverse();
    assert_equal(get_reverse_state(), SECOND_REVERSE);
    motor_drive_reverse();
    assert_equal(get_reverse_state(), REVERSE_DONE);
    motor_drive_reverse();
    assert_equal(get_reverse_state(), NO_REVERSE);
}

void mock_rpm_interrupts(uint8_t interrupt_numbers)
{
    for (int i = 0; i < interrupt_numbers; i++)
    {
        increment_wheel_ticks();
    }
    return;
}

void rpm_parametric_tests(uint16_t speed_value)
{
    // To start off, RPM should be 0
    assert_equal(get_wheel_ticks(), 0);
    
    // Pretend we fired up the RPM interuupts X amount of times
    mock_rpm_interrupts(speed_value);
    assert_equal(get_wheel_ticks(), speed_value);
    assert_equal(get_wheel_rpm_in_1Hz(), speed_value);

    // Should have reset back to 0 after the 1Hz callback
    assert_equal(get_wheel_ticks(), 0);
}

Ensure(rpm_check)
{  
    for (int i = 0; i < (2 * NORMAL_FORWARD_TICKS); i++)
    {
        rpm_parametric_tests(i);
    }
}

void normal_speed_UT(void)
{
    // Scenario A - Normal Speed

    // 0. To start off, RPM should be 0
    assert_equal(get_wheel_ticks(), 0);

    // 1. We just started running
    motor_drive_forward();

    // 2. Normal run should start at 2nd gear
    assert_equal(get_motor_momentum(), FORWARD_SECOND_GEAR);

    // 3. We somehow got to speed NORMAL_FORWARD_TICKS. So we should still be at 2nd gear
    mock_rpm_interrupts(NORMAL_FORWARD_TICKS - 1);
    speed_check(get_wheel_rpm_in_1Hz());
    assert_equal(get_motor_momentum(), FORWARD_SECOND_GEAR);
}

void fast_speed_UT_1(void)
{
    // Scenario B - Fast Speed

    // 0. To start off, RPM should be 0
    assert_equal(get_wheel_ticks(), 0);

    // 1. We just started running
    motor_drive_forward();

    // 2. Normal run should start at 2nd gear
    assert_equal(get_motor_momentum(), FORWARD_SECOND_GEAR);

    // 3. We somehow got to speed > FAST_SPEED. So we should gear down to 1st gear
    mock_rpm_interrupts(FAST_SPEED + 1);
    speed_check(get_wheel_rpm_in_1Hz());
    assert_equal(get_motor_momentum(), FORWARD_FIRST_GEAR);
}

void fast_speed_UT_2(void)
{
    // Scenario B - Fast Speed

    // 0. To start off, RPM should be 0
    assert_equal(get_wheel_ticks(), 0);

    // 1. We just started running fast
    motor_drive_forward_faster();

    // 2. Fast run should start at 3rd gear
    assert_equal(get_motor_momentum(), FORWARD_THIRD_GEAR);

    // 3. We somehow got to speed > FAST_SPEED. So we should gear down to 2nd gear
    mock_rpm_interrupts(FAST_SPEED + 1);
    speed_check(get_wheel_rpm_in_1Hz());
    assert_equal(get_motor_momentum(), FORWARD_SECOND_GEAR);
}

void fast_speed_UT_3(void)
{
    // Scenario B - Fast Speed

    // 0. To start off, RPM should be 0
    assert_equal(get_wheel_ticks(), 0);

    // 1. We just started running slow
    motor_drive_forward_slower();

    // 2. Slow run should start at 1st gear
    assert_equal(get_motor_momentum(), FORWARD_FIRST_GEAR);

    // 3. We somehow got to speed > FAST_SPEED. But that's the last gear so we shouldn't change gears
    mock_rpm_interrupts(FAST_SPEED + 1);
    speed_check(get_wheel_rpm_in_1Hz());
    assert_equal(get_motor_momentum(), FORWARD_FIRST_GEAR);
}

void slow_speed_UT_1(void)
{
    // Scenario C - Slow Speed
    // 0. To start off, RPM should be 0
    assert_equal(get_wheel_ticks(), 0);

    // 1. We just started running
    motor_drive_forward();

    // 2. Normal run should start at 2nd gear
    assert_equal(get_motor_momentum(), FORWARD_SECOND_GEAR);

    // 3. We somehow got to speed < SLOW_SPEED. So we should gear up to 3rd gear
    mock_rpm_interrupts(SLOW_SPEED - 1);
    
    // 4. But first we should wait for 5 times before we gear down
    for (int i = 0; i < (WAIT_FOR_SLOW_SPEED_COUNT + 1); i++)
    {
        printf("Speed Iteration: %d\n", i);
        assert_equal(get_motor_momentum(), FORWARD_SECOND_GEAR);
        speed_check(get_wheel_rpm_in_1Hz());
        mock_rpm_interrupts(SLOW_SPEED - 1);
    }

    // 5. Now that we passed 5 times in a row in slow speed, we should gear up
    assert_equal(get_motor_momentum(), FORWARD_THIRD_GEAR);

    // Doing this extra step as a tear down to reset the wheel ticks to 0 
    speed_check(get_wheel_rpm_in_1Hz());
}

void slow_speed_UT_2(void)
{
    // Scenario C - Slow Speed
    // 0. To start off, RPM should be 0
    assert_equal(get_wheel_ticks(), 0);

    // 1. We just started running slow
    motor_drive_forward_slower();

    // 2. Slow run should start at 1st gear
    assert_equal(get_motor_momentum(), FORWARD_FIRST_GEAR);

    // 3. We somehow got to speed < SLOW_SPEED. So we should gear up to 2nd gear
    mock_rpm_interrupts(SLOW_SPEED - 1);
    
    // 4. We should gear up after a speed check
    speed_check(get_wheel_rpm_in_1Hz());
    assert_equal(get_motor_momentum(), FORWARD_SECOND_GEAR);
}

void slow_speed_UT_3(void)
{
    // Scenario C - Slow Speed
    // 0. To start off, RPM should be 0
    assert_equal(get_wheel_ticks(), 0);

    // 1. We just started running fast
    motor_drive_forward_faster();

    // 2. fast run should start at 1st gear
    assert_equal(get_motor_momentum(), FORWARD_THIRD_GEAR);

    // 3. We somehow got to speed < SLOW_SPEED. So we should gear up to 2nd gear
    mock_rpm_interrupts(SLOW_SPEED - 1);
    
    // 4. Even though we are running slow but there are no more gears to change
    // So we should still stay at the same gear
    speed_check(get_wheel_rpm_in_1Hz());
    assert_equal(get_motor_momentum(), FORWARD_THIRD_GEAR);
}

Ensure(speed_check)
{
    
    normal_speed_UT();

    fast_speed_UT_1();
    fast_speed_UT_2();
    fast_speed_UT_3();

    slow_speed_UT_1();
    slow_speed_UT_2();
    slow_speed_UT_3();
    
}

TestSuite *ecu_motor_unittest()
{    
    TestSuite *suite = create_test_suite();
    add_test(suite, basic_state_machine_setting_check);
    add_test(suite, runtime_check);
    add_test(suite, rpm_check);
    add_test(suite, speed_check);
    // LCD Checks
    add_test(suite, LCD_speed_check);
    add_test(suite, LCD_gps_check);
    return suite;
}

int main(int argc, char **argv) 
{
    TestSuite *suite = create_test_suite();
    add_suite(suite, ecu_motor_unittest());

    return run_test_suite(suite, create_text_reporter());
}
