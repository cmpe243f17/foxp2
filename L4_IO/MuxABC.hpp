/*
 * Mux Abstact Base Class
 * @author jtran
 * @version 1.0.0
 */


#ifndef MUXABC_HPP
#define MUXABC_HPP

#include <stdbool.h>
#include <stdint.h>


class MuxABC
{
public:
    virtual void select(uint8_t sel) = 0;
    virtual uint8_t get_size(void) = 0;
};


#endif  // MUXABC_HPP
