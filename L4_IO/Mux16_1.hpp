/*
 * Mux 16-1 HP4067
 * @author jtran
 * @version 1.0.0
 */

#ifndef MUX16_1_HPP
#define MUX16_1_HPP

#include "MuxABC.hpp"

#include <stdint.h>
#include <stdbool.h>

#include "gpio.hpp"


class Mux16_1 : public MuxABC
{
public:
    Mux16_1(LPC1758_GPIO_Type sel0, LPC1758_GPIO_Type sel1, LPC1758_GPIO_Type sel2, LPC1758_GPIO_Type sel3, LPC1758_GPIO_Type enable);

    virtual void select(uint8_t sel);
    virtual uint8_t get_size(void);
  
    void enable(void);
    void disable(void);

private:
    GPIO *s0;
    GPIO *s1;
    GPIO *s2;
    GPIO *s3;
    GPIO *en;
};


#endif  // MUX16_1_HPP
