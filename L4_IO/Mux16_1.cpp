#include "Mux16_1.hpp"

#include <cstddef>

#define ENABLE_L(gpio_ptr) (gpio_ptr->setLow())
#define DISABLE_L(gpio_ptr) (gpio_ptr->setHigh())

#define MUX_SIZE 16U

Mux16_1::Mux16_1(LPC1758_GPIO_Type sel0, LPC1758_GPIO_Type sel1, LPC1758_GPIO_Type sel2, LPC1758_GPIO_Type sel3, LPC1758_GPIO_Type enable)
{
    LPC1758_GPIO_Type s[] = {sel0, sel1, sel2, sel3};
    for (uint8_t idx=0U; idx<sizeof(s); idx++) {
        GPIO *sel = new GPIO(s[idx]);
        switch (idx) {
            case 0:
                this->s0 = sel;
                break;
            case 1:
                this->s1 = sel;
                break;
            case 2:
                this->s2 = sel;
                break;
            case 3:
                this->s3 = sel;
                break;
            default:
                this->s0 = sel;
        }
        sel->setAsOutput();
        sel->setLow();
    }
    this->en = new GPIO(enable);
    this->en->setAsOutput();
    DISABLE_L(this->en);
}

void Mux16_1::select(uint8_t sel)
{
    GPIO* s[] = {this->s0, this->s1, this->s2, this->s3};
    for (uint8_t idx=0U; idx<sizeof(s)/sizeof(GPIO*); idx++) {
        bool state = ((sel & (1U << idx)) > 0U);
        if (state) {
            s[idx]->setHigh();
        }
        else {
            s[idx]->setLow();
        }
    }
}

uint8_t Mux16_1::get_size(void)
{
    return (uint8_t)MUX_SIZE;
}

void Mux16_1::enable(void)
{
    ENABLE_L(this->en);
}

void Mux16_1::disable(void)
{
    DISABLE_L(this->en);
}
