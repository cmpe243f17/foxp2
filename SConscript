from collections import namedtuple
from fnmatch import fnmatch
import os
import re
import shutil
from subprocess import Popen, PIPE

from cfiles import search_cfiles
from helper import is_windows, is_linux

__author__ = "jtran"
__version__ = "1.6.1"


"""
CLI Arguments
"""
AddOption("--ecu",
          type="string",
          metavar="<str>",
          default=None,
          help="Specify a ECU to build")

AddOption("--unit_test",
          action="store_true",
          metavar="<bool>",
          help="Invoke unit tests")

ECU_NAME = GetOption("ecu")
UNIT_TEST_ENABLED = GetOption("unit_test")


"""
Define naming conventions
"""
COMPONENT_NAME = os.path.basename(Dir(".").abspath)
GENERIC_TARGET_NAME = ("{}_{}".format(COMPONENT_NAME, ECU_NAME)) if ECU_NAME is not None else (COMPONENT_NAME)


"""
Define file nodes and directory nodes
"""
PROJECT_DIR = Dir(".")
BUILD_DIR = Dir("build")
VARIANT_DIR = (BUILD_DIR.Dir(ECU_NAME)) if ECU_NAME is not None else (BUILD_DIR.Dir(PROJECT_DIR.name))
OBJ_DIR = VARIANT_DIR.Dir("obj")
DBC_FILE = PROJECT_DIR.File("_can_dbc/243.dbc")
UNIT_TEST_DIR = PROJECT_DIR.Dir("test")
UNIT_TEST_BUILD_DIR = UNIT_TEST_DIR.Dir("build")
CGREEN_DIR = PROJECT_DIR.Dir("cgreen")
CYGWIN_DIR = PROJECT_DIR.Dir("cygwin")

INCLUDE_DIRS = [
    Dir("."),
    Dir("newlib"),
    Dir("L0_LowLevel"),
    Dir("L1_FreeRTOS"),
    Dir("L1_FreeRTOS/include"),
    Dir("L1_FreeRTOS/portable"),
    Dir("L1_FreeRTOS/portable/no_mpu"),
    Dir("L2_Drivers"),
    Dir("L2_Drivers/base"),
    Dir("L3_Utils"),
    Dir("L3_Utils/tlm"),
    Dir("L4_IO"),
    Dir("L4_IO/fat"),
    Dir("L4_IO/wireless"),
    Dir("L5_Application"),
]

INCLUDE_DIRS_ROOT = [
    Dir("L5_Application/FOXP2"),
]

SRC_DIRS = [
    Dir("newlib"),
    Dir("L0_LowLevel"),
    Dir("L1_FreeRTOS"),
    Dir("L2_Drivers"),
    Dir("L3_Utils"),
    Dir("L4_IO"),
    Dir("L5_Application"),
]

STARTUP_FILES = [
]

LINKER_FILES = [
    File("loader.ld")
]

EXCLUDED_SRC_FILES = [
    File("L1_FreeRTOS/portable/mpu/port.c")
]

MAP_FILE = VARIANT_DIR.File("{}.map".format(GENERIC_TARGET_NAME))


"""
FW versioning
"""
def get_git_hash():
    return Popen('git describe --tags --always', stdout=PIPE, shell=True).stdout.read().strip()

def get_git_branch():
    return Popen('git rev-parse --abbrev-ref HEAD', stdout=PIPE, shell=True).stdout.read().strip()

GIT_HASH_TAG = get_git_hash()
GIT_BRANCH = get_git_branch() 
print("Building from branch: " + GIT_BRANCH)
print("Git commit: " + GIT_HASH_TAG) 


"""
Define build environments
"""
arm_env = Environment(
    ENV=os.environ,
    tools=["mingw"],
    CC="arm-none-eabi-gcc",
    CXX="arm-none-eabi-g++",
    LD="arm-none-eabi-ld",
    AS="arm-none-eabi-as",
    OBJCOPY="arm-none-eabi-objcopy",
    OBJDUMP="arm-none-eabi-objdump",
    SIZE="arm-none-eabi-size",
    CFLAGS=[
        "-std=gnu11",
    ],
    CXXFLAGS=[
        "-std=gnu++11",
        "-fabi-version=0",
        "-fno-exceptions",
    ],
    LINKFLAGS=[
        "-Wl,-Map,{}".format(MAP_FILE.abspath),
        "-nostartfiles",
        "-specs=nano.specs",
        "-Xlinker",
        "--gc-sections",
    ],
    LIBPATH=[],
    CPPPATH=[],
    CPPDEFINES=[
        "BUILD_CFG_MPU=0",
        "FW_VERSION={}".format(GIT_HASH_TAG),
        "BRANCH_INFO={}".format(GIT_BRANCH),
    ],
    _SRC_DIRS=[],
    _SRC_FILES=[],
)

dbc_env = Environment(
    ENV=os.environ,
    DBCPARSER=PROJECT_DIR.File("_can_dbc/dbc_parse.py"),
    NODE="" if ECU_NAME is None else ECU_NAME.upper(),
)

arm_env.VariantDir(variant_dir=VARIANT_DIR, src_dir=Dir("."), duplicate=0)


"""
Define additional flags
"""
COMMON_CFLAGS = [
    "-g",
]
arm_env["CFLAGS"].extend(COMMON_CFLAGS)
arm_env["CXXFLAGS"].extend(COMMON_CFLAGS)

COMMON_FLAGS = [
    "-mcpu=cortex-m3",
    "-Os",
    "-mthumb",
    "-Wall",
    "-fmessage-length=0",
    "-ffunction-sections",
    "-fdata-sections",
    "-Wshadow",
    "-Wlogical-op",
    "-Wfloat-equal",
]
arm_env["CFLAGS"].extend(COMMON_FLAGS)
arm_env["CXXFLAGS"].extend(COMMON_FLAGS)
arm_env["LINKFLAGS"].extend(COMMON_FLAGS)


"""
ECU-specific: Define
"""
if ECU_NAME is not None:
    arm_env["CPPDEFINES"].append("{}_{}".format("ECU", ECU_NAME.upper()))


"""
Define builders
"""
def objcopy_generator(source, target, env, for_signature):
    fmt = "binary" if target[0].name.endswith(".bin") else "ihex"
    return "$OBJCOPY -O {} {} {}".format(fmt, source[0], target[0])
arm_env["BUILDERS"]["Objcopy"] = Builder(generator=objcopy_generator)


def objdump_generator(source, target, env, for_signature):
    return "$OBJDUMP --source --all-headers --demangle --line-numbers --wide {} > {}".format(source[0], target[0])
arm_env["BUILDERS"]["Objdump"] = Builder(generator=objdump_generator)


arm_env["BUILDERS"]["Size"] = Builder(action="$SIZE --format=berkeley $SOURCE")


def dbc_generator(source, target, env, for_signature):
    return "python $DBCPARSER -i {} -s $NODE > {}".format(source[0], target[0])
dbc_env["BUILDERS"]["Dbc"] = Builder(generator=dbc_generator)


"""
Search and group build files
"""
for linker_file in LINKER_FILES:
    arm_env["LINKFLAGS"].append("-T{}".format(File(linker_file).abspath))

for dir in SRC_DIRS:
    src_filenodes, src_dirnodes, _ = search_cfiles(dir)
    arm_env["_SRC_FILES"].extend(src_filenodes)
    arm_env["_SRC_DIRS"].extend(src_dirnodes)

arm_env["CPPPATH"].extend(INCLUDE_DIRS)

for dir in INCLUDE_DIRS_ROOT:
    _, _, include_dirnodes = search_cfiles(dir)
    arm_env["CPPPATH"].extend(include_dirnodes)

arm_env["_SRC_FILES"].extend(STARTUP_FILES)

arm_env["_SRC_FILES"] = filter(lambda fn: fn.abspath not in map(lambda _fn: _fn.abspath, EXCLUDED_SRC_FILES), arm_env["_SRC_FILES"])
Depends(MAP_FILE, arm_env["_SRC_FILES"])


"""
ECU-specific: Filter build files
"""
if ECU_NAME in ["sensor", "motor", "gateway", "geographical", "bridge"]:
    ecu_root_dirnode = Dir("L5_Application/FOXP2")
    shared_dirnode = Dir("L5_Application/FOXP2/Shared")

    src_filenodes, _, _ = search_cfiles(ecu_root_dirnode)
    arm_env["_FILTERED_SRC_FILES"] = filter(lambda fn: File(fn) not in src_filenodes, map(lambda _fn: File(_fn), arm_env["_SRC_FILES"]))
    arm_env["_FILTERED_SRC_FILES"].extend(Glob(os.path.join(ecu_root_dirnode.abspath, "*.c*")))  # Include source files in root directory only

    ecu_src_filenodes = []
    if ECU_NAME == "sensor":
        ecu_src_filenodes, _, _ = search_cfiles(Dir("L5_Application/FOXP2/Sensor"))
    elif ECU_NAME == "motor":
        ecu_src_filenodes, _, _ = search_cfiles(Dir("L5_Application/FOXP2/Motor"))
    elif ECU_NAME == "gateway":
        ecu_src_filenodes, _, _ = search_cfiles(Dir("L5_Application/FOXP2/Gateway"))
    elif ECU_NAME == "geographical":
        ecu_src_filenodes, _, _ = search_cfiles(Dir("L5_Application/FOXP2/Geographical"))
    elif ECU_NAME == "bridge":
        ecu_src_filenodes, _, _ = search_cfiles(Dir("L5_Application/FOXP2/Bridge"))
    else:
        pass
    arm_env["_FILTERED_SRC_FILES"].extend(ecu_src_filenodes)

    # Also include shared files
    shared_src_filenodes, _, _ = search_cfiles(shared_dirnode)
    arm_env["_FILTERED_SRC_FILES"].extend(shared_src_filenodes)

"""
Generate C header from DBC
"""
if ECU_NAME is not None:
    arm_env["CPPPATH"].append(VARIANT_DIR)
    dbc_filenodes = dbc_env.Dbc(target=VARIANT_DIR.File("generated_can.h"), source=DBC_FILE)
    Depends(dbc_filenodes, DBC_FILE)


"""
Perform builds
"""
obj_filenodes = []
for src_filenode in arm_env.get("_FILTERED_SRC_FILES", arm_env["_SRC_FILES"]):
    src_filename_no_ext, ext = src_filenode.name.split(".")
    dest_filepath = OBJ_DIR.File("{}.o".format(src_filename_no_ext))
    obj_filenode_list = arm_env.Object(target=dest_filepath, source=src_filenode)
    obj_filenodes.extend(obj_filenode_list)

elf_filenodes = arm_env.Program(target=VARIANT_DIR.File("{}.elf".format(GENERIC_TARGET_NAME)), source=obj_filenodes)
Depends(elf_filenodes, LINKER_FILES)

bin_filenodes = arm_env.Objcopy(target=VARIANT_DIR.File("{}.bin".format(GENERIC_TARGET_NAME)), source=elf_filenodes[0])

hex_filenodes = arm_env.Objcopy(target=VARIANT_DIR.File("{}.hex".format(GENERIC_TARGET_NAME)), source=elf_filenodes[0])

lst_filenodes = arm_env.Objdump(target=VARIANT_DIR.File("{}.lst".format(GENERIC_TARGET_NAME)), source=elf_filenodes[0])

arm_env.Size(os.path.relpath(elf_filenodes[0].abspath))


"""
Unit test - environment
"""
ut_env = Environment(
    ENV=os.environ,
    tools=["mingw"],
    CC="gcc",
    CXX="g++",
    CFLAGS=[
        "-std=gnu11",
    ],
    CXXFLAGS=[
        "-std=gnu++11",
    ],
    LINKFLAGS=[
        "-static-libgcc",
        "-static-libstdc++",
    ],
    CPPPATH=[
        CGREEN_DIR.Dir("include"),
    ] + arm_env["CPPPATH"] + arm_env["_SRC_DIRS"],
    CPPDEFINES=[
        "UNIT_TEST=1",
    ] + arm_env["CPPDEFINES"],
    LIBPATH=[
        CGREEN_DIR.Dir("lib"),
    ],
    LIBS=[
        "-lcgreen",
    ],
    _SRC_DIRS=arm_env["_SRC_DIRS"],
    _SRC_FILES=arm_env["_SRC_FILES"],
)

if is_windows():  # Inject cygwin toolchain to the beginning of PATH
    os.environ["PATH"] = os.pathsep.join([Dir("c:/cygwin/bin").abspath, os.environ["PATH"]])
    ut_env["ENV"] = os.environ
    ut_env["CC"] = File("c:/cygwin/bin/gcc")
    ut_env["CXX"] = File("c:/cygwin/bin/g++")
else:
    os.environ["LD_LIBRARY_PATH"] = os.environ.get("LD_LIBRARY_PATH", "")

# Libraries
env_var = "PATH" if is_windows() else "LD_LIBRARY_PATH"
os.environ[env_var] += "{}{}".format(os.pathsep, CYGWIN_DIR.Dir("lib").abspath)
os.environ[env_var] += "{}{}".format(os.pathsep, CGREEN_DIR.Dir("lib").abspath)
ut_env["ENV"] = os.environ
if is_linux():
    ut_env["LIBPATH"] += os.environ["LD_LIBRARY_PATH"].split(os.pathsep)


"""
Unit test - search and group files
"""
ut_prefix = "test_"

ut_src_filenodes = []
src_filenodes, _, _ = search_cfiles(UNIT_TEST_DIR)
ut_src_filenodes.extend(src_filenodes)
ut_src_filenodes = filter(lambda filenode: filenode.name.startswith(ut_prefix), ut_src_filenodes)  # Keep all source files with test_ prepended to the file name

UnitTestSrcGroup = namedtuple("UnitTestSrcGroup", ["ut_filenode", "aux_filenodes"])
ut_src_groups = []
for filenode in ut_src_filenodes:
    aux_src_filenodes = filter(lambda fn: fnmatch(os.path.splitext(filenode.name)[0].replace(ut_prefix, ""), os.path.splitext(fn.name)[0]), ut_env["_SRC_FILES"])
    new_ut_src_group = UnitTestSrcGroup(ut_filenode=filenode, aux_filenodes=aux_src_filenodes)
    ut_src_groups.append(new_ut_src_group)


"""
Unit test - perform build and execute
"""
if UNIT_TEST_ENABLED:
    for ut_src_group in ut_src_groups:
        obj_filenodes = []
        for src_file in [ut_src_group.ut_filenode] + ut_src_group.aux_filenodes:
            obj_target = UNIT_TEST_BUILD_DIR.File("{}.o".format(os.path.splitext(src_file.name)[0]))
            objs = ut_env.Object(target=obj_target, source=src_file)
            obj_filenodes.extend(objs)

        exe_target = UNIT_TEST_BUILD_DIR.File("{}{}".format(os.path.splitext(ut_src_group.ut_filenode.name)[0], (".exe" if is_windows() else "")))
        exes = ut_env.Program(target=exe_target, source=obj_filenodes)
        out = ut_env.Command(target=UNIT_TEST_BUILD_DIR.File("{}_output.txt".format(os.path.splitext(exes[0].name)[0])), source=exes, action=["$SOURCE > $TARGET"])
        Depends(out, ut_src_filenodes)
